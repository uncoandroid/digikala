package com.example.digikala.Activity_SignUp.MVVM;

import com.example.digikala.Model.Message.RegisterMessage;
import com.example.digikala.Retrofit.ApiProvider;

import io.reactivex.Single;

public class SignUpApiService implements SignUpDataSource{
    @Override
    public Single<RegisterMessage> get_SingUp_SDS(String email, String password) {
        return ApiProvider.apiProvider().get_SignUp_php(email,password);
    }
}
