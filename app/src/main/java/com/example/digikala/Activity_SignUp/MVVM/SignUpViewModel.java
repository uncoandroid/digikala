package com.example.digikala.Activity_SignUp.MVVM;

import com.example.digikala.Activity_Main.MVVM.UserLoginInfo;
import com.example.digikala.G;
import com.example.digikala.Model.Message.RegisterMessage;

import io.reactivex.Single;

public class SignUpViewModel {
    private SignUpRepository signUpRepository = new SignUpRepository();
    private UserLoginInfo userLoginInfo = new UserLoginInfo(G.context);

    public Single<RegisterMessage> get_SingUp_SVM(String email , String password){
        return  signUpRepository.get_SingUp_SDS(email , password);
    }

    public void saveUserRegistery (String email){
        userLoginInfo.save_UserEmail(email);
    }
}
