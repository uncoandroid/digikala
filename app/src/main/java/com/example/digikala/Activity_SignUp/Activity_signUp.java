package com.example.digikala.Activity_SignUp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.digikala.Activity_Main.MainActivity;
import com.example.digikala.Activity_SignUp.MVVM.SignUpViewModel;
import com.example.digikala.Model.Message.RegisterMessage;
import com.example.digikala.R;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class Activity_signUp extends AppCompatActivity {

    ImageView img_close;
    EditText edt_email, edt_password;
    Button btn_SignUp;
    String email , password;
    SignUpViewModel signUpViewModel = new SignUpViewModel();
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);


        setupView();

    }

    private void observeForSignUp() {

        email = edt_email.getText().toString();
        password = edt_password.getText().toString();

        signUpViewModel.get_SingUp_SVM(email,password).subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<RegisterMessage>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(RegisterMessage registerMessage) {
                       signUpViewModel.saveUserRegistery(registerMessage.getMessage());
                       finish();
                       MainActivity.drawerLayout.closeDrawer(Gravity.RIGHT);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                });
    }

    private void setupView() {
        btn_SignUp = findViewById(R.id.btn_SignUp);
        btn_SignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               if (edt_email.getText().toString().equals("")){
                   edt_email.setError("فیلد ایمیل نباید خالی باشد");
               }else if (edt_password.getText().toString().equals("")){
                edt_password.setError("فیلد کلمه عبور نباید خالی باشد");
               }else {
                   observeForSignUp();
               }


            }
        });
        edt_email = findViewById(R.id.edt_email_SignUp);
        edt_password = findViewById(R.id.edt_password_SignUp);
        img_close = findViewById(R.id.img_close_sign_up);
        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                MainActivity.drawerLayout.closeDrawer(Gravity.RIGHT);
            }
        });
    }

    @Override
    public void onBackPressed() {
        compositeDisposable.dispose();
        super.onBackPressed();
    }
}
