package com.example.digikala.Activity_SignUp.MVVM;

import com.example.digikala.Model.Message.RegisterMessage;

import io.reactivex.Single;

public interface SignUpDataSource {

    Single<RegisterMessage> get_SingUp_SDS(String email , String password);
}
