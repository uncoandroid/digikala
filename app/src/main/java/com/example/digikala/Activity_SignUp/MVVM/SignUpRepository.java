package com.example.digikala.Activity_SignUp.MVVM;

import com.example.digikala.Model.Message.RegisterMessage;

import io.reactivex.Single;

public class SignUpRepository implements SignUpDataSource {

    private SignUpApiService apiSignUpDataSource = new SignUpApiService();

        @Override
    public Single<RegisterMessage> get_SingUp_SDS(String email, String password) {
        return apiSignUpDataSource.get_SingUp_SDS(email,password);
    }
}
