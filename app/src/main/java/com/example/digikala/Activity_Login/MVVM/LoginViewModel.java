package com.example.digikala.Activity_Login.MVVM;

import com.example.digikala.Model.Message.RegisterMessage;

import io.reactivex.Single;

public class LoginViewModel {

    LoginRepository loginRepository = new LoginRepository();

    public Single<RegisterMessage> get_SignInMessage_LVM(String email , String password){
        return  loginRepository.get_signIn_LDS(email,password);
    }

}
