package com.example.digikala.Activity_Login.MVVM;

import com.example.digikala.Model.Message.RegisterMessage;
import com.example.digikala.Retrofit.ApiProvider;

import io.reactivex.Single;

public class LoginApiService implements LoginDataSource {


    @Override
    public Single<RegisterMessage> get_signIn_LDS(String email, String password) {
        return ApiProvider.apiProvider().get_SignIn_php(email,password);
    }
}
