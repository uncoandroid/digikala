package com.example.digikala.Activity_Login.MVVM;

import com.example.digikala.Model.Message.RegisterMessage;

import io.reactivex.Single;

public class LoginRepository implements LoginDataSource {

     LoginApiService apiLoginDataSource = new LoginApiService();

    @Override
    public Single<RegisterMessage> get_signIn_LDS(String email, String password) {
        return apiLoginDataSource.get_signIn_LDS(email,password);
    }
}
