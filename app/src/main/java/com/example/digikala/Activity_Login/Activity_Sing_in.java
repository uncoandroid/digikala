package com.example.digikala.Activity_Login;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.digikala.Activity_Login.MVVM.LoginViewModel;
import com.example.digikala.Activity_Main.MainActivity;
import com.example.digikala.Activity_SignUp.Activity_signUp;
import com.example.digikala.G;
import com.example.digikala.Model.Message.RegisterMessage;
import com.example.digikala.R;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class Activity_Sing_in extends AppCompatActivity {

    Button btn_signIn;
    ImageView img_close;
    TextView txt_signUp;
    CheckBox checkBox;
    EditText edt_email, edt_pass;
    LoginViewModel loginViewModel = new LoginViewModel();
    CompositeDisposable compositeDisposable = new CompositeDisposable();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sing_in);

        setupView();


    }

    private void observeForSignIn() {
        String email = edt_email.getText().toString();
        String password = edt_pass.getText().toString();

        loginViewModel.get_SignInMessage_LVM(email,password).subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<RegisterMessage>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(RegisterMessage signInMessage) {
                       if (!signInMessage.getMessage().equals("not found")){
                           Intent intent = new Intent();
                           intent.putExtra("email",signInMessage.getMessage());
                           setResult(RESULT_OK,intent);
                           finish();
                       }else{
                           Toast.makeText(Activity_Sing_in.this, "نام کاربری یا شناسه عبور نادرست است", Toast.LENGTH_SHORT).show();
                       }
                    }

                    @Override
                    public void onError(Throwable e) {


                    }
                });
    }


    private void setupView() {

        btn_signIn = findViewById(R.id.btn_signIn);
        btn_signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                observeForSignIn();
            }
        });
        edt_email = findViewById(R.id.edt_email_signIn);
        edt_pass = findViewById(R.id.edt_password_signIn);

        checkBox = findViewById(R.id.checkBox);
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!isChecked) {
                    edt_pass.setTransformationMethod(PasswordTransformationMethod.getInstance());
                } else {
                    edt_pass.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                }
            }
        });

        txt_signUp = findViewById(R.id.txt_signUp);
        txt_signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(G.context, Activity_signUp.class);
                startActivity(intent);
                finish();
            }
        });
        img_close = findViewById(R.id.img_close_sign_in);
        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                MainActivity.drawerLayout.closeDrawer(Gravity.RIGHT);
            }
        });
    }

    @Override
    protected void onDestroy() {
        compositeDisposable.dispose();
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        MainActivity.drawerLayout.closeDrawer(Gravity.RIGHT);
    }
}
