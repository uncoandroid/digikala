package com.example.digikala.Activity_Login.MVVM;

import com.example.digikala.Model.Message.RegisterMessage;

import io.reactivex.Single;

public interface LoginDataSource {

    Single<RegisterMessage> get_signIn_LDS(String email , String password);
}
