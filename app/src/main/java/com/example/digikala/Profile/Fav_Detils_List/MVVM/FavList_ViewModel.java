package com.example.digikala.Profile.Fav_Detils_List.MVVM;

import com.example.digikala.Model.Favorite;
import com.example.digikala.Model.Message.Messages;

import java.util.List;

import io.reactivex.Single;

public class FavList_ViewModel implements FavList_DataSource {
    FavList_Repository favList_repository = new FavList_Repository();

    @Override
    public Single<List<Favorite>> get_Favorite_FDS(String email) {
        return favList_repository.get_Favorite_FDS(email);
    }

    @Override
    public Single<Messages> delete_Favorites_FDS(String id) {
        return favList_repository.delete_Favorites_FDS(id);
    }
}
