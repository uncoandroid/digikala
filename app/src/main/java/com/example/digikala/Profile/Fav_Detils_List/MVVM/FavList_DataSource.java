package com.example.digikala.Profile.Fav_Detils_List.MVVM;

import com.example.digikala.Model.Favorite;
import com.example.digikala.Model.Message.Messages;

import java.util.List;

import io.reactivex.Single;

public interface FavList_DataSource {

    Single<List<Favorite>> get_Favorite_FDS(String email);
    Single<Messages> delete_Favorites_FDS(String id);
}
