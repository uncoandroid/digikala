package com.example.digikala.Profile.Fav_Detils_List.MVVM;

import com.example.digikala.Model.Favorite;
import com.example.digikala.Model.Message.Messages;
import com.example.digikala.Retrofit.ApiProvider;
import com.example.digikala.Retrofit.ApiService;

import java.util.List;

import io.reactivex.Single;

public class FavList_ApiService implements FavList_DataSource {
    ApiService apiService = ApiProvider.apiProvider();

    @Override
    public Single<List<Favorite>> get_Favorite_FDS(String email) {
        return apiService.get_Favorites(email);
    }

    @Override
    public Single<Messages> delete_Favorites_FDS(String id) {
        return apiService.delete_Favorites(id);
    }
}
