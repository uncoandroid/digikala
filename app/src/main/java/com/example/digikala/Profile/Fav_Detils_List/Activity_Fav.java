package com.example.digikala.Profile.Fav_Detils_List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.digikala.Adapter.Adapter_FavList;
import com.example.digikala.Detail.Activity_ProductDetails;
import com.example.digikala.G;
import com.example.digikala.Model.Favorite;
import com.example.digikala.Model.Message.Messages;
import com.example.digikala.Profile.Fav_Detils_List.MVVM.FavList_ViewModel;
import com.example.digikala.R;

import java.util.List;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class Activity_Fav extends AppCompatActivity {

    CompositeDisposable compositeDisposable = new CompositeDisposable();
    FavList_ViewModel favList_viewModel = new FavList_ViewModel();
    RecyclerView recyclerView;
    String b_email;
    ImageView close;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity__fav);

        b_email = getIntent().getExtras().getString("user_email");

        setupView();
        observeForFav();
    }

    private void observeForFav() {
        favList_viewModel.get_Favorite_FDS(b_email)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<List<Favorite>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(List<Favorite> favorites) {


                        recyclerView.setAdapter(new Adapter_FavList(favorites, new Adapter_FavList.OnFavioriteClick() {
                            @Override
                            public void onFavClick(String productID, String title, String image) {
                                Intent intent = new Intent(G.context, Activity_ProductDetails.class);
                                intent.putExtra("id", productID);
                                intent.putExtra("title", title);
                                intent.putExtra("image", image);
                                intent.putExtra("email",b_email );
                                startActivity(intent);
                            }
                        }, new Adapter_FavList.OnDeleteClick() {
                            @Override
                            public void onDelete(String favID) {
                                favList_viewModel.delete_Favorites_FDS(favID)
                                        .subscribeOn(Schedulers.newThread())
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe(new SingleObserver<Messages>() {
                                            @Override
                                            public void onSubscribe(Disposable d) {
                                                compositeDisposable.add(d);
                                            }

                                            @Override
                                            public void onSuccess(Messages messages) {
                                                Toast.makeText(Activity_Fav.this, "محصول از لیست علاقه مند ها حدف شد", Toast.LENGTH_SHORT).show();
                                                Log.i("LOG", "onSuccess: " + messages.getStatus());
                                            }

                                            @Override
                                            public void onError(Throwable e) {

                                            }
                                        });
                            }
                        }));

                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                });
    }

    private void setupView() {
        close = findViewById(R.id.img_closeFav);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        recyclerView = findViewById(R.id.rv_favList);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    protected void onDestroy() {
        compositeDisposable.dispose();
        super.onDestroy();
    }
}
