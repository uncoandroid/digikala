package com.example.digikala.Profile.Fav_Detils_List.MVVM;

import com.example.digikala.Model.Favorite;
import com.example.digikala.Model.Message.Messages;

import java.util.List;

import io.reactivex.Single;

public class FavList_Repository implements FavList_DataSource {
    FavList_ApiService favList_apiService = new FavList_ApiService();

    @Override
    public Single<List<Favorite>> get_Favorite_FDS(String email) {
        return favList_apiService.get_Favorite_FDS(email);
    }

    @Override
    public Single<Messages> delete_Favorites_FDS(String id) {
        return favList_apiService.delete_Favorites_FDS(id);
    }
}
