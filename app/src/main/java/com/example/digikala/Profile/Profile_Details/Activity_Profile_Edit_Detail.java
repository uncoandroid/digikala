package com.example.digikala.Profile.Profile_Details;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatSpinner;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.digikala.G;
import com.example.digikala.Model.Message.EditProfileMessage;
import com.example.digikala.Profile.Profile_Details.MVVM.Edit_profile_ViewModel;
import com.example.digikala.R;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class Activity_Profile_Edit_Detail extends AppCompatActivity {

    EditText edt_editProfile_email, edt_editProfile_name, edt_editProfile_family, edt_editProfile_code,edt_editProfile_adress, edt_editProfile_tell,edt_editProfile_mobile;
    CheckBox Ch_khabarname;
    AppCompatSpinner spinner_editProfile_day, spinner_editProfile_month, spinner_editProfile_year;
    RadioButton radio_editProfile_woman, radio_editProfile_man;
    RadioGroup radioGroup_editProfile;
    Button btn_editProfile_submit;
    String[] yearsSpinner, monthsSpinner, daysSpinner;
    ImageView img_close;
    String b_user_email;

    String daySelected, monthSelected, yearSelected;

    Edit_profile_ViewModel edit_profile_viewModel = new Edit_profile_ViewModel();
    CompositeDisposable compositeDisposable = new CompositeDisposable();

    String edt_email, edt_name, edt_family, edt_code_meli, edt_address,edt_tel,edt_mobile;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity__profile__edit__detail);

        Bundle bundle = getIntent().getExtras();
        b_user_email = bundle.getString("user_email");

        setupView();
        read_spinner_year_month_day();




    }




    private void setupView() {
        img_close=findViewById(R.id.img_editProfile_back);
        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        edt_editProfile_name = findViewById(R.id.edt_editProfile_name);
        edt_editProfile_family = findViewById(R.id.edt_editProfile_family);
        edt_editProfile_code = findViewById(R.id.edt_editProfile_code);
        edt_editProfile_tell = findViewById(R.id.edt_editProfile_tell);
        edt_editProfile_mobile = findViewById(R.id.edt_editProfile_mobile);
        edt_editProfile_email = findViewById(R.id.edt_editProfile_email);
        edt_editProfile_adress = findViewById(R.id.edt_editProfile_adress);

        Ch_khabarname = findViewById(R.id.chb_editProfile_emailReceive);
        spinner_editProfile_day = findViewById(R.id.spinner_editProfile_day);
        spinner_editProfile_month = findViewById(R.id.spinner_editProfile_month);
        spinner_editProfile_year = findViewById(R.id.spinner_editProfile_year);
        radioGroup_editProfile = findViewById(R.id.radioGroup_editProfile);
        radio_editProfile_woman = findViewById(R.id.radio_editProfile_woman);
        radio_editProfile_man = findViewById(R.id.radio_editProfile_man);
        btn_editProfile_submit = findViewById(R.id.btn_editProfile_submit);

        /*************************************************************************/
        edt_editProfile_email.setText(b_user_email);
        /*************************************************************************/

        btn_editProfile_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final int selectedRadioButtom = radioGroup_editProfile.getCheckedRadioButtonId();
                RadioButton radioSelectedBottom = findViewById(selectedRadioButtom);
                edit_profile_viewModel.updpateProfile_EDS(
                        edt_email = edt_editProfile_email.getText().toString(),
                        edt_name = edt_editProfile_name.getText().toString(),
                        edt_family = edt_editProfile_family.getText().toString(),
                        edt_code_meli = edt_editProfile_code.getText().toString(),
                        edt_tel = edt_editProfile_tell.getText().toString(),
                        edt_mobile = edt_editProfile_mobile.getText().toString(),
                        daySelected + "/" + monthSelected + "/" + yearSelected,
                        edt_address = edt_editProfile_adress.getText().toString(),
                        radioSelectedBottom.getText().toString().equals("مرد") ? 1 : 0,
                        Ch_khabarname.isChecked() ? 1 : 0,
                        0)
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new SingleObserver<EditProfileMessage>() {
                            @Override
                            public void onSubscribe(Disposable d) {
                                compositeDisposable.add(d);
                            }

                            @Override
                            public void onSuccess(EditProfileMessage editProfileMessage) {

                                Toast.makeText(Activity_Profile_Edit_Detail.this, "ویرایش انجام شد", Toast.LENGTH_SHORT).show();
                                finish();

                           }

                            @Override
                            public void onError(Throwable e) {

                            }
                        });


            }
        });

    }

    private void read_spinner_year_month_day() {
        yearsSpinner = getResources().getStringArray(R.array.year);
        monthsSpinner = getResources().getStringArray(R.array.month);
        daysSpinner = getResources().getStringArray(R.array.days);


        ArrayAdapter yearsAdapter = new ArrayAdapter(G.context, R.layout.spinner_custom_textcolor, yearsSpinner);
        spinner_editProfile_year.setAdapter(yearsAdapter);
        spinner_editProfile_year.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                yearSelected = parent.getItemAtPosition(position).toString();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        ArrayAdapter monthsAdapter = new ArrayAdapter(G.context, R.layout.spinner_custom_textcolor, monthsSpinner);
        spinner_editProfile_month.setAdapter(monthsAdapter);
        spinner_editProfile_month.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                monthSelected = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        ArrayAdapter daysAdapter = new ArrayAdapter(G.context, R.layout.spinner_custom_textcolor, daysSpinner);
        spinner_editProfile_day.setAdapter(daysAdapter);
        spinner_editProfile_day.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                daySelected = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }

    @Override
    protected void onDestroy() {
        compositeDisposable.dispose();
        super.onDestroy();
    }
}
