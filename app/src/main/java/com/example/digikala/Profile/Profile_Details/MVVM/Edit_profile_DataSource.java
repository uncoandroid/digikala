package com.example.digikala.Profile.Profile_Details.MVVM;

import com.example.digikala.Model.Message.EditProfileMessage;

import io.reactivex.Single;

public interface Edit_profile_DataSource {

    Single<EditProfileMessage> updpateProfile_EDS(String email,String nik_name,String family , String code_meli ,String tellephone,String mobile,String tavalod,String address, int jensiat ,int khabarname , int level);
}
