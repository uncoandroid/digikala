package com.example.digikala.Profile.Profile_Details.MVVM;

import com.example.digikala.Model.Message.EditProfileMessage;
import com.example.digikala.Retrofit.ApiProvider;
import com.example.digikala.Retrofit.ApiService;

import io.reactivex.Single;

public class Edit_profile_ApiService implements Edit_profile_DataSource {
    ApiService apiService = ApiProvider.apiProvider();


    @Override
    public Single<EditProfileMessage> updpateProfile_EDS(String email, String nik_name, String family, String code_meli, String tellephone, String mobile, String tavalod, String address, int jensiat, int khabarname, int level) {
        return apiService.editProfile_php(email,nik_name,family,code_meli,tellephone,mobile,tavalod,address,jensiat,khabarname,level);
    }
}
