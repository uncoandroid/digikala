package com.example.digikala.Profile.Profile_Details.MVVM;

import com.example.digikala.Model.Message.EditProfileMessage;

import io.reactivex.Single;

public class Edit_profile_ViewModel implements Edit_profile_DataSource {
    Edit_profile_Repository edit_profile_repository = new Edit_profile_Repository();


    @Override
    public Single<EditProfileMessage> updpateProfile_EDS(String email, String nik_name, String family, String code_meli, String tellephone, String mobile, String tavalod, String address, int jensiat, int khabarname, int level) {
        return edit_profile_repository.updpateProfile_EDS(email,nik_name,family,code_meli,tellephone,mobile,tavalod,address,jensiat,khabarname,level);
    }
}
