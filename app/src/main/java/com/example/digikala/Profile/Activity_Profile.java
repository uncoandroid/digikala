package com.example.digikala.Profile;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.digikala.Activity_Main.MVVM.MainViewModel;
import com.example.digikala.Activity_Main.MainActivity;
import com.example.digikala.G;
import com.example.digikala.Model.Message.Messages;
import com.example.digikala.Profile.Fav_Detils_List.Activity_Fav;
import com.example.digikala.Profile.Profile_Details.Activity_Profile_Edit_Detail;
import com.example.digikala.R;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static com.example.digikala.Detail.Activity_ProductDetails.txt_basketCount;

public class Activity_Profile extends AppCompatActivity {

    ImageView img_close;
    TextView txt_email_profile;
    String User_Email;
    Button btnExit;
    MainViewModel mainViewModel = new MainViewModel();
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    LinearLayout lin_profile_editProfile,lin_fav_list;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);


        User_Email = MainActivity.txt_login_navigation.getText().toString();

        setupView();
    }


    private void setupView() {
        img_close=findViewById(R.id.img_close);
        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        txt_email_profile=findViewById(R.id.txt_email_profile);
        txt_email_profile.setText(User_Email);
        lin_profile_editProfile=findViewById(R.id.lin_profile_editProfile);
        lin_profile_editProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(G.context, Activity_Profile_Edit_Detail.class);
                intent.putExtra("user_email",User_Email);
                startActivity(intent);
            }
        });
        btnExit = findViewById(R.id.btn_exit_user);
        btnExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainViewModel.remove_UserEmail(User_Email);
                Intent intent = new Intent(G.context,MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
        lin_fav_list=findViewById(R.id.lin_fav_list);
        lin_fav_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(G.context, Activity_Fav.class);
                intent.putExtra("user_email",User_Email);
                startActivity(intent);
            }
        });


    }

    @Override
    protected void onDestroy() {
        compositeDisposable.dispose();
        super.onDestroy();
    }
}
