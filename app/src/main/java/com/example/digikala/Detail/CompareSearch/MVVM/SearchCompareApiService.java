package com.example.digikala.Detail.CompareSearch.MVVM;

import com.example.digikala.Model.Product;
import com.example.digikala.Retrofit.ApiProvider;
import com.example.digikala.Retrofit.ApiService;

import java.util.List;

import io.reactivex.Single;

public class SearchCompareApiService implements SearchCompareDataSource {
    private ApiService apiService = ApiProvider.apiProvider();
    @Override
    public Single<List<Product>> get_SearchProductCompare(String search) {
        return apiService.get_SearchProduct_php(search);
    }
}
