package com.example.digikala.Detail.CompareProduct.MVVM;

import com.example.digikala.Model.Properties;

import java.util.List;

import io.reactivex.Single;

public class CompareRepository implements CompareDataSource {

    CompareApiService compareRepository = new CompareApiService();
    @Override
    public Single<List<Properties>> getCompareDataSource() {
        return compareRepository.getCompareDataSource();
    }
}
