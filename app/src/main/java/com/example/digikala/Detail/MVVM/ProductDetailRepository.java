package com.example.digikala.Detail.MVVM;

import com.example.digikala.Model.DetailProduct;
import com.example.digikala.Model.Message.Messages;

import java.util.List;

import io.reactivex.Single;

public class ProductDetailRepository implements ProductDetailDataSoource{

    ProductDetailApiService aPiProductDetailDataSource = new ProductDetailApiService();

    @Override
    public Single<List<DetailProduct>> get_DetailProduct_PDS(String id, String user) {
        return aPiProductDetailDataSource.get_DetailProduct_PDS(id,user);
    }

    @Override
    public Single<Messages> addToBasket_PDS(String product_id, String email) {
        return aPiProductDetailDataSource.addToBasket_PDS(product_id,email);
    }

    @Override
    public Single<Messages> getbasketcount_PDS(String email) {
        return aPiProductDetailDataSource.getbasketcount_PDS(email);
    }
}
