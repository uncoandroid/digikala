package com.example.digikala.Detail.Fav.MVVM;

import com.example.digikala.Model.Message.Messages;

import io.reactivex.Single;

public class FavRepository implements FavDataSource {

    FavApiService favApiService = new FavApiService();
    @Override
    public Single<Messages> addFavorites_FDS(String email, String id, int parent, String title) {
        return favApiService.addFavorites_FDS(email,id,parent,title);
    }
}
