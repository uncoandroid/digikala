package com.example.digikala.Detail.Chart.MVVM;

import com.example.digikala.Model.HistoryModel;

import java.util.List;

import io.reactivex.Single;

public class ChartViewModel {
    ChartRepository chartRepository = new ChartRepository();

    public Single<List<HistoryModel>> get_HistoryModel_CVM(String id) {
        return chartRepository.get_HistoryModel_CDS(id);
    }
}
