package com.example.digikala.Detail.properties.MVVM;

import com.example.digikala.Model.Properties;
import com.example.digikala.Retrofit.ApiProvider;

import java.util.List;

import io.reactivex.Single;

public class PropertiesApiService implements PropertiesDataSource {
    @Override
    public Single<List<Properties>> get_properties_PDS() {
        return ApiProvider.apiProvider().get_properties_php();
    }
}
