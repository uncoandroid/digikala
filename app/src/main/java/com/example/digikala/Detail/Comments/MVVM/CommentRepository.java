package com.example.digikala.Detail.Comments.MVVM;

import com.example.digikala.Model.Comment;
import com.example.digikala.Model.Message.Like_disLikeMessage;

import java.util.List;

import io.reactivex.Single;

public class CommentRepository implements CommentDataSource {

    CommentApiService commentApiService = new CommentApiService();



    @Override
    public Single<List<Comment>> get_Comment_CDS(String id) {
        return commentApiService.get_Comment_CDS(id);
    }

    @Override
    public Single<Like_disLikeMessage> get_LikeComment(String id) {
        return commentApiService.get_LikeComment(id);
    }

    @Override
    public Single<Like_disLikeMessage> get_disLikeComment(String id) {
        return commentApiService.get_disLikeComment(id);
    }
}
