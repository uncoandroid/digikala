package com.example.digikala.Detail.Fav.MVVM;

import com.example.digikala.Model.Message.Messages;

import io.reactivex.Single;

public interface FavDataSource {
    Single<Messages> addFavorites_FDS(String email, String id, int parent, String title);
}
