package com.example.digikala.Detail.Comments.MVVM;

import com.example.digikala.Model.Comment;
import com.example.digikala.Model.Message.Like_disLikeMessage;

import java.util.List;

import io.reactivex.Single;

public class CommentViewModel {
    CommentRepository commentRepository = new CommentRepository();

    public Single<List<Comment>> get_Comment_CVM(String id) {
        return commentRepository.get_Comment_CDS(id);
    }

    public Single<Like_disLikeMessage> get_LikeOrDisLike(Comment comment , String likeOrdisLike){

        if (likeOrdisLike.equals("like")){
            return commentRepository.get_LikeComment(comment.getId());
        }else{
            return commentRepository.get_disLikeComment(comment.getId());
        }
    }
}

