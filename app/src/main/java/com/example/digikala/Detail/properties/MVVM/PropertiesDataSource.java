package com.example.digikala.Detail.properties.MVVM;

import com.example.digikala.Model.Properties;

import java.util.List;

import io.reactivex.Single;

public interface PropertiesDataSource {

    Single<List<Properties>> get_properties_PDS();
}
