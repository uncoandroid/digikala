package com.example.digikala.Detail.CompareSearch.MVVM;

import com.example.digikala.Model.Product;

import java.util.List;

import io.reactivex.Single;

public class SearchCompareRepository implements SearchCompareDataSource {
    SearchCompareApiService searchCompareApiService = new SearchCompareApiService();

    @Override
    public Single<List<Product>> get_SearchProductCompare(String search) {
        return searchCompareApiService.get_SearchProductCompare(search);
    }
}
