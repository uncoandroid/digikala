package com.example.digikala.Detail.WriteComment;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.digikala.G;
import com.example.digikala.R;

public class Activity_WriteComment extends AppCompatActivity {

    ImageView img_close;
    EditText edtTitle, edtPositive, edtNegative, edtPassage;
    Button submit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_write_comment);
        setupView();

    }

    private void setupView() {
        edtTitle = findViewById(R.id.edt_title_naghdVabareesi);
        edtPositive = findViewById(R.id.edt_positive_naghdVabareesi);
        edtNegative = findViewById(R.id.edt_negative_naghdVabareesi);
        edtPassage = findViewById(R.id.edt_passage_naghdVabareesi);
        img_close = findViewById(R.id.img_close);
        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              finish();
            }
        });
        submit = findViewById(R.id.btn_submit_naghdVabareesi);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra("title", edtTitle.getText().toString());
                intent.putExtra("positive", edtPositive.getText().toString());
                intent.putExtra("negative", edtNegative.getText().toString());
                intent.putExtra("passage", edtPassage.getText().toString());
                setResult(RESULT_OK, intent);
                finish();
            }
        });
    }


}
