package com.example.digikala.Detail.Comments.MVVM;

import com.example.digikala.Model.Comment;
import com.example.digikala.Model.Message.Like_disLikeMessage;
import com.example.digikala.Retrofit.ApiProvider;

import java.util.List;

import io.reactivex.Single;

public class CommentApiService implements CommentDataSource {
    @Override
    public Single<List<Comment>> get_Comment_CDS(String id) {
        return ApiProvider.apiProvider().getComment_php(id);
    }

    @Override
    public Single<Like_disLikeMessage> get_LikeComment(String id) {
        return ApiProvider.apiProvider().get_Like_php(id);
    }

    @Override
    public Single<Like_disLikeMessage> get_disLikeComment(String id) {
        return ApiProvider.apiProvider().get_disLike_php(id);
    }
}
