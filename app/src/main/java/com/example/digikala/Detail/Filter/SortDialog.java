package com.example.digikala.Detail.Filter;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RadioButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.example.digikala.G;
import com.example.digikala.R;

public class SortDialog extends DialogFragment {
    private static final int MOST_VIEW = 1;
    private static final int MOST_SELL = 2;
    private static final int PRICE_DESC = 3;
    private static final int PRICE_ASC = 4;
    private static final int NEWEST = 5;

    public static int situation = 1;

    onDialogItemClick onDialogItemClick;


    RadioButton radioButton_sort_1, radioButton_sort_2, radioButton_sort_3, radioButton_sort_4, radioButton_sort_5;
    LinearLayout lin_sort_1, lin_sort_2, lin_sort_3, lin_sort_4, lin_sort_5;
    View view;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        view = LayoutInflater.from(G.context).inflate(R.layout.sort_alert_dialog, null);
        setupViews();
        alertDialog.setView(view);
        return alertDialog.create();
    }

    public void setOnDialogItemClick(SortDialog.onDialogItemClick onDialogItemClick) {
        this.onDialogItemClick = onDialogItemClick;
    }

    private void setupViews() {
        radioButton_sort_1 = view.findViewById(R.id.radioButton_sort_1);
        radioButton_sort_2 = view.findViewById(R.id.radioButton_sort_2);
        radioButton_sort_3 = view.findViewById(R.id.radioButton_sort_3);
        radioButton_sort_4 = view.findViewById(R.id.radioButton_sort_4);
        radioButton_sort_5 = view.findViewById(R.id.radioButton_sort_5);


        lin_sort_1 = view.findViewById(R.id.lin_sort_1);
        lin_sort_2 = view.findViewById(R.id.lin_sort_2);
        lin_sort_3 = view.findViewById(R.id.lin_sort_3);
        lin_sort_4 = view.findViewById(R.id.lin_sort_4);
        lin_sort_5 = view.findViewById(R.id.lin_sort_5);


        switch (situation) {
            case MOST_VIEW:
                radioButton_sort_1.setChecked(true);
                radioButton_sort_2.setChecked(false);
                radioButton_sort_3.setChecked(false);
                radioButton_sort_4.setChecked(false);
                radioButton_sort_5.setChecked(false);
                break;
            case MOST_SELL:
                radioButton_sort_1.setChecked(false);
                radioButton_sort_2.setChecked(true);
                radioButton_sort_3.setChecked(false);
                radioButton_sort_4.setChecked(false);
                radioButton_sort_5.setChecked(false);
                break;
            case PRICE_DESC:
                radioButton_sort_1.setChecked(false);
                radioButton_sort_2.setChecked(false);
                radioButton_sort_3.setChecked(true);
                radioButton_sort_4.setChecked(false);
                radioButton_sort_5.setChecked(false);
                break;
            case PRICE_ASC:
                radioButton_sort_1.setChecked(false);
                radioButton_sort_2.setChecked(false);
                radioButton_sort_3.setChecked(false);
                radioButton_sort_4.setChecked(true);
                radioButton_sort_5.setChecked(false);


                break;
            case NEWEST:
                radioButton_sort_1.setChecked(false);
                radioButton_sort_2.setChecked(false);
                radioButton_sort_3.setChecked(false);
                radioButton_sort_4.setChecked(false);
                radioButton_sort_5.setChecked(true);
                break;

        }

        lin_sort_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onDialogItemClick.onClick(MOST_VIEW);
                situation = MOST_VIEW;
                dismiss();
                situation = MOST_VIEW;

            }
        });

        lin_sort_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onDialogItemClick.onClick(MOST_SELL);
                situation = MOST_SELL;
                dismiss();
                situation = MOST_SELL;
            }
        });
        lin_sort_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onDialogItemClick.onClick(PRICE_DESC);
                situation = PRICE_DESC;
                dismiss();
                situation = PRICE_DESC;
            }
        });
        lin_sort_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onDialogItemClick.onClick(PRICE_ASC);
                situation = PRICE_ASC;
                dismiss();
                situation = PRICE_ASC;
            }
        });
        lin_sort_5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onDialogItemClick.onClick(NEWEST);
                situation = NEWEST;
                dismiss();
                situation = NEWEST;
            }
        });

    }

    public interface onDialogItemClick {
        void onClick(int sort);
    }
}
