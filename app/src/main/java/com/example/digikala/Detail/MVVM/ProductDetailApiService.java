package com.example.digikala.Detail.MVVM;

import com.example.digikala.Detail.properties.MVVM.PropertiesDataSource;
import com.example.digikala.Model.DetailProduct;
import com.example.digikala.Model.Message.Messages;
import com.example.digikala.Model.Properties;
import com.example.digikala.Retrofit.ApiProvider;
import com.example.digikala.Retrofit.ApiService;

import java.util.List;

import io.reactivex.Single;

public class ProductDetailApiService implements ProductDetailDataSoource {

    ApiService apiService = ApiProvider.apiProvider();
    @Override
    public Single<List<DetailProduct>> get_DetailProduct_PDS(String id, String user) {
        return apiService.get_detailsProduct_php(id , user);
    }

    @Override
    public Single<Messages> addToBasket_PDS(String product_id, String email) {
        return apiService.addToBasket(product_id,email);
    }

    @Override
    public Single<Messages> getbasketcount_PDS(String email) {
        return apiService.getbasketcount_php(email);
    }
}
