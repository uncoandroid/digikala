package com.example.digikala.Detail.AddComment.MVVM;

import com.example.digikala.Model.Message.CommentMessage;
import com.example.digikala.Model.RatingModel;

import java.util.List;

import io.reactivex.Single;

public class AddCommentRepository {
    AddCommentAPiService addCommentAPiService = new AddCommentAPiService();

    public Single<CommentMessage> sendPoint_ACS(List<RatingModel> ratingModels) {
        return addCommentAPiService.sendPoint_ADS(ratingModels);
    }
    public Single<CommentMessage> WriteComment_ACS(String title, String positive, String negative, String passage,String user,String idproduct) {
        return addCommentAPiService.WriteComment_ADS(title,positive,negative,passage,user,idproduct);
    }
}
