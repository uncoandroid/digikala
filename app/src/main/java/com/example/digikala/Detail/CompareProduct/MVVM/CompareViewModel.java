package com.example.digikala.Detail.CompareProduct.MVVM;

import com.example.digikala.Model.Properties;

import java.util.List;

import io.reactivex.Single;

public class CompareViewModel {

    CompareRepository compareRepository = new CompareRepository();

    public Single<List<Properties>> getCompareViewModel(){
        return compareRepository.getCompareDataSource();
    }
}
