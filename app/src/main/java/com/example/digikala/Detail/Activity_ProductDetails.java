package com.example.digikala.Detail;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.ColorUtils;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.core.widget.NestedScrollView;
import androidx.core.widget.TextViewCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.digikala.Activity_Login.Activity_Sing_in;
import com.example.digikala.Activity_Main.MVVM.MainViewModel;
import com.example.digikala.Adapter.Adapter_ProductDetails_Rating;
import com.example.digikala.Basket.Activity_BasketList;
import com.example.digikala.Detail.AddComment.Activity_SetPointComment;
import com.example.digikala.Detail.Chart.Activity_Chart;
import com.example.digikala.Detail.Comments.Activity_Comments;
import com.example.digikala.Detail.CompareProduct.Activity_Compare;
import com.example.digikala.Detail.Fav.FavoriteBottomSheet;
import com.example.digikala.Detail.Fav.MVVM.FavViewModel;
import com.example.digikala.Detail.Fav.NewFoolderDialog;
import com.example.digikala.Detail.MVVM.ProductDetailViewModel;
import com.example.digikala.Detail.properties.Activity_Properties;
import com.example.digikala.G;
import com.example.digikala.Model.DetailFavorite;
import com.example.digikala.Model.DetailProduct;
import com.example.digikala.Model.Favorite;
import com.example.digikala.Model.Message.Messages;
import com.example.digikala.Model.RatingModel;
import com.example.digikala.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static com.example.digikala.Activity_Main.MainActivity.txt_login_navigation;

public class Activity_ProductDetails extends AppCompatActivity {


    //*****************
    TextView title, subTitle, txt_color, txt_price, txt_description, txt_point_rate, toolbar_title, txtMore, txtDescription;
    ImageView imglogo, img_share_product, toolbar_basket, toolbar_back, img_toolbar_menu, img_fav_product;
    int toolbarMergeColor, toolbarDrawableMergeColor, toolbarBasketCountMergeColor, toolbarTitleYPosiotion = -1;
    ProductDetailViewModel detailViewModel = new ProductDetailViewModel();
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    LinearLayout lin_properties_click, lin_comment_click,toolbar;
    List<DetailFavorite> favoriteList = new ArrayList<>();
    List<RatingModel> ratingModelList = new ArrayList<>();
    MainViewModel mainViewModel = new MainViewModel();
    FavViewModel favViewModel = new FavViewModel();
    public static final int SET_TRANSLATION_Y = 58;
    public static TextView txt_basketCount;
    RelativeLayout relative_basketClick;
    public static final int LOGIN_REQUEST = 356;
    String id, b_title, b_image, b_email;
    NestedScrollView nestedScrollView;
    RecyclerView recyclerView_rating;
    Button btn_addToBasket;
    RatingBar ratingBar;
    String User_email;
    int startHeight; // baraye txtMore hast
    Thread thread;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);

        id = getIntent().getExtras().getString("id");
        b_title = getIntent().getExtras().getString("title");
        b_image = getIntent().getExtras().getString("image");
        b_email = getIntent().getExtras().getString("email");

        setupView();
        toolbar_title.setTranslationY(SET_TRANSLATION_Y); // in khat code baraye animatin toolbar title dar productDetail hast
        observeForProductDetails();
        observeForBasketCount();
    }

    private void observeForBasketCount() {
        detailViewModel.getbasketcount_PDS(detailViewModel.getUserEmail())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<Messages>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(Messages messages) {
                        if (!messages.getMessage().equals("0")) {
                            txt_basketCount.setVisibility(View.VISIBLE);
                            txt_basketCount.setText(messages.getMessage());
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                });

    }


    private void setupView() {
        relative_basketClick = findViewById(R.id.relative_basketClick);
        relative_basketClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View vf) {
                if (detailViewModel.getUserEmail().equals("")) {
                    Intent intent = new Intent(G.context, Activity_Sing_in.class);
                    startActivityForResult(intent, LOGIN_REQUEST);
                } else {
                    Intent intent = new Intent(G.context, Activity_BasketList.class);
                    intent.putExtra("basketCount", txt_basketCount.getText().toString());
                    startActivity(intent);
                }
            }
        });
        txt_basketCount = findViewById(R.id.txt_basketCount);
        img_share_product = findViewById(R.id.img_share_product);
        img_share_product = findViewById(R.id.img_share_product);
        img_share_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_TEXT, "https://www.digikala.com/");
                startActivity(Intent.createChooser(intent, "اشتراک گذاری در :"));

            }
        });
        img_fav_product = findViewById(R.id.img_fav_product);
        img_fav_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (detailViewModel.getUserEmail().equals("")) {
                    Intent intent = new Intent(G.context, Activity_Sing_in.class);
                    startActivityForResult(intent, LOGIN_REQUEST);
                } else {
                    final FavoriteBottomSheet favoriteBottomSheet = new FavoriteBottomSheet();
                    Bundle bundle = new Bundle();
                    bundle.putString("url", favoriteList.size() > 0 ? b_image : "");
                    bundle.putInt("count", favoriteList.size() > 0 ? favoriteList.size() : 0);
                    favoriteBottomSheet.setArguments(bundle);
                    favoriteBottomSheet.SetonAddFollderImageClick(new FavoriteBottomSheet.onAddFollderImageClick() {
                        @Override
                        public void onClick() {
                            final NewFoolderDialog newFoolderDialog = new NewFoolderDialog();
                            newFoolderDialog.setOnSubmitFollderClick(new NewFoolderDialog.OnSubmitFollderClick() {
                                @Override
                                public void onClick(String folderName) {
                                    newFoolderDialog.dismiss();
                                    favViewModel.addFavorites_FDS(detailViewModel.getUserEmail(), id, 1, folderName)
                                            .subscribeOn(Schedulers.newThread())
                                            .observeOn(AndroidSchedulers.mainThread())
                                            .subscribe(new SingleObserver<Messages>() {
                                                @Override
                                                public void onSubscribe(Disposable d) {
                                                    compositeDisposable.add(d);
                                                }

                                                @Override
                                                public void onSuccess(Messages messages) {
                                                    Log.i("LOG", "Success: " + messages);

                                                }

                                                @Override
                                                public void onError(Throwable e) {
                                                    Log.i("Error", "onError: " + e.toString());
                                                }
                                            });
                                }
                            });
                            newFoolderDialog.show(getSupportFragmentManager(), null);newFoolderDialog.show(getSupportFragmentManager(), null);
                        }
                    });

                    favoriteBottomSheet.SetonLinTextClick(new FavoriteBottomSheet.OnSubmitLinClick() {
                        @Override
                        public void onClick() {
                            favViewModel.addFavorites_FDS(detailViewModel.getUserEmail(), id, 0, "")
                                    .subscribeOn(Schedulers.newThread())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe(new SingleObserver<Messages>() {
                                        @Override
                                        public void onSubscribe(Disposable d) {
                                            compositeDisposable.add(d);
                                        }

                                        @Override
                                        public void onSuccess(Messages messages) {
                                            Log.i("Success", "onSuccess: " + messages.getStatus());
                                            Toast.makeText(Activity_ProductDetails.this, "محصول به علاقه مندی ها اضافه شد", Toast.LENGTH_SHORT).show();
                                            favoriteBottomSheet.dismiss();
                                        }

                                        @Override
                                        public void onError(Throwable e) {
                                            Log.i("Error", "onError: " + e.toString());
                                        }
                                    });
                        }
                    });
                    favoriteBottomSheet.show(getSupportFragmentManager(), null);

                }

            }
        });
        txtMore = findViewById(R.id.txtMore);
        txtMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (txtMore.getText().toString().equals("ادامه مطلب")) {
                    startHeight = txtDescription.getHeight();
                    txtDescription.setMaxLines(Integer.MAX_VALUE);
                    int widthSpec = View.MeasureSpec.makeMeasureSpec(txtDescription.getWidth(), View.MeasureSpec.EXACTLY);
                    int heightSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
                    txtDescription.measure(widthSpec, heightSpec);
                    int targetHeight = txtDescription.getMeasuredHeight();
                    final int heightSpan = targetHeight - startHeight;
                    txtDescription.getLayoutParams().height = startHeight;
                    txtDescription.setLayoutParams(txtDescription.getLayoutParams());
                    Animation animation = new Animation() {
                        @Override
                        protected void applyTransformation(float interpolatedTime, Transformation t) {
                            txtDescription.getLayoutParams().height = (int) (startHeight + heightSpan * interpolatedTime);
                            txtDescription.setLayoutParams(txtDescription.getLayoutParams());
                        }
                    };
                    animation.setDuration(300);
                    txtDescription.startAnimation(animation);
                    txtMore.setText("بستن");
                } else {
                    startHeight = txtDescription.getHeight();
                    txtDescription.setMaxLines(8);
                    int widthSpec = View.MeasureSpec.makeMeasureSpec(txtDescription.getWidth(), View.MeasureSpec.EXACTLY);
                    int heightSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
                    txtDescription.measure(widthSpec, heightSpec);
                    int targetHeight = txtDescription.getMeasuredHeight();
                    final int heightSpan = targetHeight - startHeight;
                    txtDescription.getLayoutParams().height = startHeight;
                    txtDescription.setLayoutParams(txtDescription.getLayoutParams());
                    Animation animation = new Animation() {
                        @Override
                        protected void applyTransformation(float interpolatedTime, Transformation t) {
                            txtDescription.getLayoutParams().height = (int) (startHeight + heightSpan * interpolatedTime);
                            txtDescription.setLayoutParams(txtDescription.getLayoutParams());
                        }
                    };
                    animation.setDuration(300);
                    txtDescription.startAnimation(animation);
                    txtMore.setText("ادامه مطلب");
                }
            }
        });
        txtDescription = findViewById(R.id.txt_description_detail);
        lin_comment_click = findViewById(R.id.lin_comment_click);
        lin_comment_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(G.context, Activity_Comments.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("id", id);
                intent.putExtra("title", b_title);
                startActivity(intent);
            }
        });
        lin_properties_click = findViewById(R.id.lin_properties_click);
        lin_properties_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(G.context, Activity_Properties.class);
                intent.putExtra("id", id);
                intent.putExtra("title", b_title);
                startActivity(intent);
            }
        });
        toolbar_title = findViewById(R.id.txt_product_toolbar_title_productDetail);
        img_toolbar_menu = findViewById(R.id.img_3point_productDetails);
        img_toolbar_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popupMenu = new PopupMenu(G.context, img_toolbar_menu, Gravity.LEFT);
                popupMenu.getMenuInflater().inflate(R.menu.detail_more_menu, popupMenu.getMenu());
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        if (item.getTitle().equals("نمودار قیمت")) {
                            Intent intent = new Intent(G.context, Activity_Chart.class);
                            intent.putExtra("id", id);
                            intent.putExtra("title", b_title);
                            startActivity(intent);
                        } else if (item.getTitle().equals("مقایسه محصول")) {
                            Intent intent = new Intent(G.context, Activity_Compare.class);
                            intent.putExtra("ProductTitle", b_title);
                            intent.putExtra("imageURL", b_image);
                            startActivity(intent);
                        }
                        return true;
                    }
                });
                popupMenu.show();
            }
        });
        toolbar_back = findViewById(R.id.img_back_productDetails);
        toolbar_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        toolbar_basket = findViewById(R.id.img_basket_productDetails);
        toolbar = findViewById(R.id.toolbar);
        nestedScrollView = findViewById(R.id.NSV_productDetails);
        nestedScrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, final int scrollY, final int oldScrollX, final int oldScrollY) {

                thread = new Thread(new Runnable() {
                    @Override
                    public void run() {

                        Log.i("LOG", "run: " + scrollY);
                        if (scrollY > 855 && scrollY > oldScrollY && scrollY < 1000) {
                            if (toolbarTitleYPosiotion > -45) {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        toolbar_title.setTranslationY(toolbarTitleYPosiotion - 5);
                                        toolbarTitleYPosiotion = toolbarTitleYPosiotion - 5;
                                    }
                                });
                            }
                        } else if (scrollY < 855 && scrollY < oldScrollY && scrollY > 400) {
                            if (toolbarTitleYPosiotion < SET_TRANSLATION_Y) {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        toolbar_title.setTranslationY(toolbarTitleYPosiotion + 5);
                                        toolbarTitleYPosiotion = toolbarTitleYPosiotion + 5;
                                    }
                                });
                            }
                        } else if (scrollY < 400) {


                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    toolbarTitleYPosiotion = SET_TRANSLATION_Y;
                                    toolbar_title.setTranslationY(toolbarTitleYPosiotion);
                                }
                            });
                        } else if (scrollY > 1000) {

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    toolbarTitleYPosiotion = -45;
                                    toolbar_title.setTranslationY(toolbarTitleYPosiotion);
                                }
                            });
                        }


                        //******************************************************************//

                        //code haye bala baraye animation  txt_toolbar_title mibashad

                        //******************************************************************//


                        if (scrollY > 50 && scrollY < 734) {

                            float ratio = scrollY / 734f;
                            toolbarMergeColor = ColorUtils.blendARGB(ContextCompat.getColor(Activity_ProductDetails.this, R.color.colorWhite), ContextCompat.getColor(Activity_ProductDetails.this, R.color.colorPrimary), ratio);
                            toolbarDrawableMergeColor = ColorUtils.blendARGB(ContextCompat.getColor(G.context, R.color.Gray500), ContextCompat.getColor(G.context, R.color.colorWhite), ratio);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    toolbar.setBackgroundColor(toolbarMergeColor);
                                    DrawableCompat.setTint(toolbar_back.getDrawable(), toolbarDrawableMergeColor);
                                    DrawableCompat.setTint(toolbar_basket.getDrawable(), toolbarDrawableMergeColor);
                                    DrawableCompat.setTint(img_toolbar_menu.getDrawable(), toolbarDrawableMergeColor);

                                }
                            });
                        } else if (scrollY < 50 && oldScrollY > scrollY) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    toolbar.setBackgroundColor(ContextCompat.getColor(G.context, R.color.colorWhite));
                                }
                            });
                        } else if (scrollY > 734) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    toolbar.setBackgroundColor(ContextCompat.getColor(G.context, R.color.colorPrimary));
                                }
                            });
                        }
                    }
                });
                thread.start();
            }
        });
        imglogo = findViewById(R.id.img_products_Details);
        title = findViewById(R.id.txt_productDetails_title);
        subTitle = findViewById(R.id.txt_Sub_productDetails_title);
        txt_color = findViewById(R.id.txt_color);
        txt_price = findViewById(R.id.txt_price_details);
        txt_description = findViewById(R.id.txt_description_detail);
        txt_point_rate = findViewById(R.id.txt_point_details);
        ratingBar = findViewById(R.id.ratingbar_detailsproduct);
        recyclerView_rating = findViewById(R.id.rv_productDetails_rating);
        recyclerView_rating.setLayoutManager(new LinearLayoutManager(G.context, LinearLayoutManager.VERTICAL, false));

        btn_addToBasket = findViewById(R.id.btn_addToBasket);
        btn_addToBasket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (detailViewModel.getUserEmail().equals("")) {
                    Intent intent = new Intent(G.context, Activity_Sing_in.class);
                    startActivityForResult(intent, LOGIN_REQUEST);
                } else {

                    detailViewModel.addToBasket_PDS(id, detailViewModel.getUserEmail())
                            .subscribeOn(Schedulers.newThread())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(new SingleObserver<Messages>() {
                                @Override
                                public void onSubscribe(Disposable d) {
                                    compositeDisposable.add(d);
                                }

                                @Override
                                public void onSuccess(Messages messages) {

                                    if (messages.getStatus().equals("success")) {
                                        int Current_basketCount = Integer.parseInt(txt_basketCount.getText().toString());
                                        Current_basketCount++;
                                        txt_basketCount.setText(Current_basketCount + "");
                                        txt_basketCount.setVisibility(View.VISIBLE);
                                        Toast.makeText(Activity_ProductDetails.this, "محصول به سبد خرید اضافه شد", Toast.LENGTH_SHORT).show();
                                    }
                                }

                                @Override
                                public void onError(Throwable e) {

                                }
                            });

                }


            }
        });


    }

    private void observeForProductDetails() {
        detailViewModel.get_DetailProduct_PDS(id, detailViewModel.getUserEmail().equals("") ? "" : detailViewModel.getUserEmail())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<List<DetailProduct>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onSuccess(List<DetailProduct> detailProducts) {


                        favoriteList = detailProducts.get(0).getFavoriteList();
                        Picasso.get().load(detailProducts.get(0).getImage()).into(imglogo);
                        txt_color.setText(detailProducts.get(0).getColors());
                        title.setText(detailProducts.get(0).getTitle());

                        subTitle.setText(detailProducts.get(0).getTitle());
                        txt_price.setText(detailProducts.get(0).getPrice());
                        txt_description.setText(Html.fromHtml(detailProducts.get(0).getIntroduction()));
                        txt_point_rate.setText(detailProducts.get(0).getRating() + " از 5 ");
                        ratingBar.setRating(Float.parseFloat(detailProducts.get(0).getRating()));
                        toolbar_title.setText(detailProducts.get(0).getTitle());

                        try {
                            JSONArray ratingItemArray = new JSONArray(detailProducts.get(0).getRatingItem());
                            for (int i = 0; i < ratingItemArray.length(); i++) {
                                RatingModel ratingModel = new RatingModel();
                                JSONObject jsonObject = ratingItemArray.getJSONObject(i);
                                String title = jsonObject.getString("title");
                                String value = jsonObject.getString("value");

                                ratingModel.setTitle(title);
                                ratingModel.setValue(value);

                                ratingModelList.add(ratingModel);
                            }
                            recyclerView_rating.setAdapter(new Adapter_ProductDetails_Rating(ratingModelList));


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.i("LOG", "onError: " + e.toString());

                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == LOGIN_REQUEST && resultCode == RESULT_OK) {
            User_email = data.getExtras().getString("email");
            txt_login_navigation.setText(User_email);
            txt_login_navigation.setBackgroundColor(Color.TRANSPARENT);
            mainViewModel.saveUserEmail(User_email);
            observeForBasketCount();
        }

    }


    @Override
    protected void onDestroy() {
        compositeDisposable.dispose();
        super.onDestroy();
    }
}
