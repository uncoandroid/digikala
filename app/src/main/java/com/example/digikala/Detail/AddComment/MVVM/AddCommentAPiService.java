package com.example.digikala.Detail.AddComment.MVVM;

import com.example.digikala.Model.Message.CommentMessage;
import com.example.digikala.Model.RatingModel;
import com.example.digikala.Retrofit.ApiProvider;
import com.example.digikala.Retrofit.ApiService;


import java.util.List;

import io.reactivex.Single;

public class AddCommentAPiService {

    private ApiService apiService = ApiProvider.apiProvider();

    public Single<CommentMessage> sendPoint_ADS(List<RatingModel> ratingModels) {
        return apiService.sendPoint_php(ratingModels);
    }

    public Single<CommentMessage> WriteComment_ADS(String title, String positive, String negative, String passage,String user,String idproduct) {
        return apiService.sendWriteCommentParam_php(title,positive,negative,passage,user,idproduct);
    }
}
