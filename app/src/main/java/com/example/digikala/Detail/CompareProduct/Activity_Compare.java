package com.example.digikala.Detail.CompareProduct;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.digikala.Adapter.Adapter_AddCompare;
import com.example.digikala.Adapter.Adapter_ProductCompare;
import com.example.digikala.Detail.CompareProduct.MVVM.CompareViewModel;
import com.example.digikala.Detail.CompareSearch.Activity_CompareSearch;
import com.example.digikala.G;
import com.example.digikala.Model.Properties;
import com.example.digikala.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class Activity_Compare extends AppCompatActivity {

    ImageView imgClose, img_AddProductCompare, img_Product1;
    RecyclerView recyclerView1;
    String b_imageURL, b_txt_product1Compare;
    TextView txt_product1Compare, txt_secondProductAdd;
    LinearLayout lin_highlightAddProduct;

    List<Properties> propertiesList;

    CompareViewModel compareViewModel = new CompareViewModel();
    CompositeDisposable compositeDisposable = new CompositeDisposable();

    List<Properties> SecondpropertiesList;
    Adapter_ProductCompare adapter_productCompare;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compare);

        b_imageURL = getIntent().getExtras().getString("imageURL");
        b_txt_product1Compare = getIntent().getExtras().getString("ProductTitle");
        setupView();
        observeForCompare();
    }

    private void observeForCompare() {
        compareViewModel.getCompareViewModel()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<List<Properties>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(List<Properties> properties) {

                        adapter_productCompare = new Adapter_ProductCompare(properties);
                        recyclerView1.setAdapter(adapter_productCompare);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                });
    }

    private void setupView() {
        propertiesList = new ArrayList<>();
        SecondpropertiesList = new ArrayList<>();
        txt_secondProductAdd = findViewById(R.id.txt_secondProductAdd);
        lin_highlightAddProduct = findViewById(R.id.lin_highlightAddProduct);
        txt_product1Compare = findViewById(R.id.txt_product1Compare);
        img_AddProductCompare = findViewById(R.id.img_AddProductCompare);
        img_AddProductCompare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(G.context, Activity_CompareSearch.class);
                startActivityForResult(intent, 100);
            }
        });
        txt_product1Compare.setText(b_txt_product1Compare);
        img_Product1 = findViewById(R.id.img_Product1);
        Picasso.get().load(b_imageURL).into(img_Product1);

        recyclerView1 = findViewById(R.id.rv_compareList);
        recyclerView1.setLayoutManager(new LinearLayoutManager(G.context, LinearLayoutManager.VERTICAL, false));

        imgClose = findViewById(R.id.img_close);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 100 && resultCode == RESULT_OK) {
            propertiesList.clear();
            Picasso.get().load(data.getExtras().getString("image")).into(img_AddProductCompare);
            lin_highlightAddProduct.setVisibility(View.VISIBLE);
            txt_secondProductAdd.setVisibility(View.VISIBLE);
            txt_secondProductAdd.setText(data.getExtras().getString("title"));
            String list = data.getExtras().getString("properties");
            try {
                JSONArray jsonArray = new JSONArray(list);
                for (int i = 0; i < jsonArray.length(); i++) {
                    Properties properties = new Properties();
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    properties.setTitle(jsonObject.getString("title"));
                    properties.setValue(jsonObject.getString("value"));
                    properties.setSecond(jsonObject.getString("second"));
                    propertiesList.add(properties);
                }
                adapter_productCompare.bindSecondProduct(propertiesList);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onDestroy() {
        compositeDisposable.dispose();
        super.onDestroy();
    }
}
