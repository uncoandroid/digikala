package com.example.digikala.Detail.Chart;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.digikala.Detail.Chart.MVVM.ChartViewModel;
import com.example.digikala.G;
import com.example.digikala.Model.HistoryModel;
import com.example.digikala.R;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class Activity_Chart extends AppCompatActivity {

    ImageView img_close;
    LineChart lineChart;
    String b_Id,b_title;
    List<HistoryModel> My_historyModels;
    ChartViewModel chartViewModel = new ChartViewModel();
    CompositeDisposable compositeDisposable = new CompositeDisposable();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chart);

        Bundle bundle = getIntent().getExtras();
        b_Id = bundle.getString("id");
        b_title = bundle.getString("title");

        setupView();
        observeForChart();
    }

    private void observeForChart() {
        chartViewModel.get_HistoryModel_CVM(b_Id)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<List<HistoryModel>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(List<HistoryModel> historyModels) {
                        if (historyModels.size()>0){

                            My_historyModels =historyModels;
                            List<Entry> values = new ArrayList<>();
                            for (int i = 0; i < historyModels.size(); i++) {
                                values.add(new Entry(i,Integer.parseInt(historyModels.get(i).getPrice())));
                            }

                            LineDataSet lineDataSet = new LineDataSet(values, "نمودار قیمت موبایل" + b_title);

                            lineDataSet.setColor(ContextCompat.getColor(G.context, R.color.colorBlue));
                            lineDataSet.setLineWidth(3f);
                            lineDataSet.setValueTextSize(10f);
                            lineDataSet.setValueTextColor(ContextCompat.getColor(G.context, R.color.colorBlack));
                            lineDataSet.setCircleColor(ContextCompat.getColor(G.context, R.color.startColor));
                            lineDataSet.setFillColor(ContextCompat.getColor(G.context, R.color.colorAccent));
                            lineDataSet.setDrawFilled(true);
                            lineDataSet.setFillDrawable(ContextCompat.getDrawable(G.context, R.drawable.chart_gradient));

                            Typeface typeface = Typeface.createFromAsset(G.context.getAssets(), "b_yekan.ttf");
                            lineDataSet.setValueTypeface(typeface);

                            List<ILineDataSet> dataSets = new ArrayList<>();
                            dataSets.add(lineDataSet);

                            LineData lineData = new LineData(dataSets);
                            lineChart.animateXY(1000,1000);
                            final XAxis xAxis = lineChart.getXAxis();
                            xAxis.setValueFormatter(new ValueFormatter() {
                                @Override
                                public String getFormattedValue(float value) {

                                    xAxis.setLabelCount(4,true);
                                    return My_historyModels.get((int)value).getDate();
                                }
                            });
                            lineChart.setData(lineData);
                        }

                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                });
    }

    private void setupView() {

        img_close = findViewById(R.id.img_close_chart);
        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        lineChart = findViewById(R.id.linChart);




    }

    @Override
    protected void onDestroy() {
        compositeDisposable.dispose();
        super.onDestroy();
    }
}
