package com.example.digikala.Detail.Fav.MVVM;

import com.example.digikala.Model.Message.Messages;
import com.example.digikala.Retrofit.ApiProvider;
import com.example.digikala.Retrofit.ApiService;

import io.reactivex.Single;

public class FavApiService implements FavDataSource {
    ApiService apiService = ApiProvider.apiProvider();

    @Override
    public Single<Messages> addFavorites_FDS(String email, String id, int parent, String title) {
        return apiService.addFaviorits_php(email,id,parent,title);
    }
}
