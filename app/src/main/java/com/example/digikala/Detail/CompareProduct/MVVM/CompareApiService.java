package com.example.digikala.Detail.CompareProduct.MVVM;

import com.example.digikala.Model.Properties;
import com.example.digikala.Retrofit.ApiClient;
import com.example.digikala.Retrofit.ApiProvider;
import com.example.digikala.Retrofit.ApiService;

import java.util.List;

import io.reactivex.Single;

public class CompareApiService implements CompareDataSource{
    private ApiService apiService = ApiProvider.apiProvider();

    @Override
    public Single<List<Properties>> getCompareDataSource()
    {
        return apiService.get_properties_php();
    }
}
