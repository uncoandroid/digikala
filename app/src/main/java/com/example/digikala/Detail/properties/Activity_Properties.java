package com.example.digikala.Detail.properties;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.example.digikala.Adapter.Adapter_Properties;
import com.example.digikala.Detail.properties.MVVM.PropertiesViewModel;
import com.example.digikala.G;
import com.example.digikala.Model.Properties;
import com.example.digikala.R;

import java.util.List;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class Activity_Properties extends AppCompatActivity {

    PropertiesViewModel propertiesViewModel = new PropertiesViewModel();
    CompositeDisposable compositeDisposable = new CompositeDisposable();

    TextView txt_title;
    RecyclerView recyclerView;
    ImageView img_close;
    String b_title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_properties);

        Bundle bundle = getIntent().getExtras();
        b_title = bundle.getString("title");

        setupView();
        observeForProperties();

    }

    private void observeForProperties() {
        propertiesViewModel.get_properties_PVM().subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<List<Properties>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(List<Properties> properties) {

                        YoYo.with(Techniques.SlideInRight)
                                .duration(500)
                                .repeat(0)
                                .playOn(findViewById(R.id.rv_properties_list));
                        recyclerView.setAdapter(new Adapter_Properties(properties));
                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                });
    }

    private void setupView() {

        txt_title = findViewById(R.id.txt_properties_title);
        txt_title.setText(b_title);
        img_close = findViewById(R.id.img_close_properties);
        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        recyclerView = findViewById(R.id.rv_properties_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(G.context, LinearLayoutManager.VERTICAL, false));
    }

    @Override
    protected void onDestroy() {
        compositeDisposable.dispose();
        super.onDestroy();
    }
}
