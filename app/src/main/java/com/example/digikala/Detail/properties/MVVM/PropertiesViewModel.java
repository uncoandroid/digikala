package com.example.digikala.Detail.properties.MVVM;

import com.example.digikala.Model.Properties;

import java.util.List;

import io.reactivex.Single;

public class PropertiesViewModel {
    PropertiesRepository propertiesRepository = new PropertiesRepository();

    public Single<List<Properties>> get_properties_PVM(){
        return  propertiesRepository.get_properties_PDS();
    }
}
