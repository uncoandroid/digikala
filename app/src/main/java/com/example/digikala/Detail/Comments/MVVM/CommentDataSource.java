package com.example.digikala.Detail.Comments.MVVM;

import com.example.digikala.Model.Comment;
import com.example.digikala.Model.Message.Like_disLikeMessage;

import java.util.List;

import io.reactivex.Single;

public interface CommentDataSource {

    Single<List<Comment>> get_Comment_CDS(String id);

    Single<Like_disLikeMessage> get_LikeComment(String id);

    Single<Like_disLikeMessage> get_disLikeComment(String id);
}
