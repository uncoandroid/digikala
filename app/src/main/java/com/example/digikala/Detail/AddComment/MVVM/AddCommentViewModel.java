package com.example.digikala.Detail.AddComment.MVVM;

import com.example.digikala.Model.Message.CommentMessage;
import com.example.digikala.Model.RatingModel;

import java.util.List;

import io.reactivex.Single;

public class AddCommentViewModel {

    private AddCommentRepository addCommentRepository = new AddCommentRepository();

    public Single<CommentMessage> sendPoint_AVD(List<RatingModel> ratingModels){
        return addCommentRepository.sendPoint_ACS(ratingModels);
    }
    public Single<CommentMessage> WriteComment_AVD(String title, String positive, String negative, String passage,String user,String idproduct) {
        return addCommentRepository.WriteComment_ACS(title,positive,negative,passage,user,idproduct);
    }
}
