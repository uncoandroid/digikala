package com.example.digikala.Detail.Filter.MVVM;

import com.example.digikala.Model.Message.FilterMessage;
import com.example.digikala.Model.Product;

import org.json.JSONObject;

import java.util.List;

import io.reactivex.Single;

public class FilterRepository implements FilterDataSoruce  {

    FilterApiService filterApiService = new FilterApiService();

    @Override
    public Single<List<Product>> getTabItem(String cat) {
        return filterApiService.getTabItem(cat);
    }

    @Override
    public Single<List<Product>> getSortProduct_FDS(String cat, int sort) {
        return filterApiService.getSortProduct_FDS(cat,sort);
    }

    @Override
    public Single<FilterMessage> sendFilterParam_FDS(List<JSONObject> jsonObjects) {
        return filterApiService.sendFilterParam_FDS(jsonObjects);
    }
}
