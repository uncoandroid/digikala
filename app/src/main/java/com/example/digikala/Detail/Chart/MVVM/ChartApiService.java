package com.example.digikala.Detail.Chart.MVVM;

import com.example.digikala.Model.HistoryModel;
import com.example.digikala.Retrofit.ApiProvider;
import com.example.digikala.Retrofit.ApiService;

import java.util.List;

import io.reactivex.Single;

public class ChartApiService implements ChartDataSource {
    private ApiService apiService = ApiProvider.apiProvider();
    @Override
    public Single<List<HistoryModel>> get_HistoryModel_CDS(String id) {
        return apiService.get_HistoryModel_php(id);
    }
}
