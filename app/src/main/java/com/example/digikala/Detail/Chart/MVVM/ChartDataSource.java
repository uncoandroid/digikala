package com.example.digikala.Detail.Chart.MVVM;

import com.example.digikala.Model.HistoryModel;

import java.util.List;

import io.reactivex.Single;

public interface ChartDataSource {

    Single<List<HistoryModel>> get_HistoryModel_CDS(String id);
}
