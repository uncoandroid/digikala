package com.example.digikala.Detail.Chart.MVVM;

import com.example.digikala.Model.HistoryModel;

import java.util.List;

import io.reactivex.Single;

public class ChartRepository implements ChartDataSource {
    ChartApiService chartApiService = new ChartApiService();

    @Override
    public Single<List<HistoryModel>> get_HistoryModel_CDS(String id) {
        return chartApiService.get_HistoryModel_CDS(id);
    }
}
