package com.example.digikala.Detail.Fav;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.example.digikala.G;
import com.example.digikala.R;

public class NewFoolderDialog extends DialogFragment {
    View view;
    TextView txt_submit_newfollder;
    EditText edt_newFoolder_Dialog;
    OnSubmitFollderClick onSubmitFollderClick;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        if (view == null) {
            view = LayoutInflater.from(G.context).inflate(R.layout.new_foolder_dialog, null);
        }
        // baraye context nabayad ax G.context estefade konim hatman bayad getContext ro entekhab konim
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        setupView();
        builder.setView(view);
        return builder.create();
    }

    public void setOnSubmitFollderClick(OnSubmitFollderClick onSubmitFollderClick){

        this.onSubmitFollderClick = onSubmitFollderClick;
    }

    private void setupView() {
        edt_newFoolder_Dialog = view.findViewById(R.id.edt_newFoolder_Dialog);
        txt_submit_newfollder = view.findViewById(R.id.txt_submit_newfollder);



        txt_submit_newfollder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSubmitFollderClick.onClick(edt_newFoolder_Dialog.getText().toString());
            }
        });
    }

    public interface OnSubmitFollderClick {
        void onClick(String folderName);
    }

}
