package com.example.digikala.Detail.Filter;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.example.digikala.Adapter.Adapter_Filter_Details_left;
import com.example.digikala.Adapter.Adapter_filter_detail_right;
import com.example.digikala.Detail.Filter.MVVM.FilterViewModel;
import com.example.digikala.G;
import com.example.digikala.Model.FilterItem;
import com.example.digikala.Model.Message.FilterMessage;
import com.example.digikala.Model.Value;
import com.example.digikala.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class Activity_filter_details extends AppCompatActivity {
    ImageView closePage;
    RecyclerView rv_filterFragment_right, rv_filterFragment_left;
    Button btn_filter;
    String b_filterParams;
    List<FilterItem> filterItemList = new ArrayList<>();
    List<JSONObject> jsonObjects = new ArrayList<>();
    Adapter_filter_detail_right rightFilterItemAdapter;
    Adapter_Filter_Details_left leftFilterItemAdapter;
    FilterViewModel filterViewModel = new FilterViewModel();
    CompositeDisposable compositeDisposable = new CompositeDisposable();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_details);

        setupViews();

        Bundle bundle = getIntent().getExtras();
        b_filterParams = bundle.getString("filterParams");
        convertParamsToJSON(b_filterParams);


        rightFilterItemAdapter = new Adapter_filter_detail_right(filterItemList, new Adapter_filter_detail_right.onFilterItem_Right_Click() {
            @Override
            public void onItemClick(List<Value> values, String title) {
                leftFilterItemAdapter.onBindValues(values, title);
            }
        });
        rv_filterFragment_right.setAdapter(rightFilterItemAdapter);


        leftFilterItemAdapter = new Adapter_Filter_Details_left(new Adapter_Filter_Details_left.OnLeftItemClickListener() {
            @Override
            public void onLeftItemClick(List<Value> values, String parent, JSONObject jsonObject) {
                jsonObjects.add(jsonObject);
                rightFilterItemAdapter.onBindCount(values, parent);
            }
        });


        //code zir barye in hast k , baraye dafeye aval , liste samte chap  khali nabashad
        leftFilterItemAdapter.onBindValues(filterItemList.get(0).getValues(), filterItemList.get(0).getTitle());
        rv_filterFragment_left.setAdapter(leftFilterItemAdapter);


    }

    private void observeSendFilterParam() {
        filterViewModel.sendFilterParam_FDS(jsonObjects)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<FilterMessage>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(FilterMessage message) {
                        Activity_Filter activity_filter = new Activity_Filter();
                        activity_filter.filterProducts_method(jsonObjects);
                        finish();
                    }

                    @Override
                    public void onError(Throwable e) {
                        finish();
                    }
                });

    }

    private void setupViews() {
        closePage = findViewById(R.id.img_close_filter2);
        closePage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        rv_filterFragment_right = findViewById(R.id.rv_filterFragment_right);
        rv_filterFragment_right.setLayoutManager(new LinearLayoutManager(G.context, RecyclerView.VERTICAL, false));

        rv_filterFragment_left = findViewById(R.id.rv_filterFragment_left);
        rv_filterFragment_left.setLayoutManager(new LinearLayoutManager(G.context, RecyclerView.VERTICAL, false));


        btn_filter = findViewById(R.id.btn_filterButton);
        btn_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                observeSendFilterParam();
            }
        });


    }

    private void convertParamsToJSON(String param) {
        try {
            JSONArray jsonArray = new JSONArray(param);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                FilterItem filterItem = new FilterItem();
                filterItem.setTitle(jsonObject.getString("title"));
                List<Value> myValues = new ArrayList<>();

                JSONArray valuesArray = jsonObject.getJSONArray("values");
                for (int j = 0; j < valuesArray.length(); j++) {
                    JSONObject valueObject = valuesArray.getJSONObject(j);
                    String value = valueObject.getString("title");
                    Value myValue = new Value();
                    myValue.setTitle(value);
                    myValue.setChecked(false);
                    myValues.add(myValue);
                }
                filterItem.setValues(myValues);
                filterItemList.add(filterItem);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        compositeDisposable.dispose();
        super.onDestroy();
    }
}
