package com.example.digikala.Detail.properties.MVVM;

import com.example.digikala.Model.Properties;

import java.util.List;

import io.reactivex.Single;

public class PropertiesRepository implements PropertiesDataSource {

    PropertiesApiService propertiesApiService = new PropertiesApiService();

    @Override
    public Single<List<Properties>> get_properties_PDS() {
        return propertiesApiService.get_properties_PDS();
    }
}
