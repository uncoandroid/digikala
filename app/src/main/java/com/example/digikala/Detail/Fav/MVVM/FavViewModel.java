package com.example.digikala.Detail.Fav.MVVM;

import com.example.digikala.Model.Message.Messages;

import io.reactivex.Single;

public class FavViewModel implements FavDataSource {

    FavRepository favRepository = new FavRepository();

    @Override
    public Single<Messages> addFavorites_FDS(String email, String id, int parent, String title) {
        return favRepository.addFavorites_FDS(email,id,parent,title);
    }
}
