package com.example.digikala.Detail.Filter.MVVM;

import com.example.digikala.Model.Message.FilterMessage;
import com.example.digikala.Model.Product;
import com.example.digikala.Retrofit.ApiProvider;
import com.example.digikala.Retrofit.ApiService;

import org.json.JSONObject;

import java.util.List;

import io.reactivex.Single;

public class FilterApiService implements FilterDataSoruce {
    ApiService apiService = ApiProvider.apiProvider();

    @Override
    public Single<List<Product>> getTabItem(String cat) {
        return apiService.get_CategoryTabItem_php(cat);
    }

    @Override
    public Single<List<Product>> getSortProduct_FDS(String cat, int sort) {
        return apiService.getSortProduct_php(cat,sort);
    }

    @Override
    public Single<FilterMessage> sendFilterParam_FDS(List<JSONObject> jsonObjects) {
        return apiService.sendFilterParam(jsonObjects);
    }
}
