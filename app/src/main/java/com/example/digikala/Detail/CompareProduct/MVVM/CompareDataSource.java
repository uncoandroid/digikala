package com.example.digikala.Detail.CompareProduct.MVVM;

import com.example.digikala.Model.Properties;

import java.util.List;

import io.reactivex.Single;

public interface CompareDataSource {

    Single<List<Properties>> getCompareDataSource();
}
