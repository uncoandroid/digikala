package com.example.digikala.Detail.CompareSearch.MVVM;

import com.example.digikala.Model.Product;

import java.util.List;

import io.reactivex.Single;

public class SearchCompareViewModel {
    SearchCompareRepository searchCompareRepository = new SearchCompareRepository();
    public Single<List<Product>> getSearcCompareProduct(String search){
        return searchCompareRepository.get_SearchProductCompare(search);
    }
}
