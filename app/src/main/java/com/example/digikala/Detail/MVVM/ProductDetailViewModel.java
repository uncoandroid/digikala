package com.example.digikala.Detail.MVVM;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.digikala.Activity_Main.MVVM.UserLoginInfo;
import com.example.digikala.G;
import com.example.digikala.Model.DetailProduct;
import com.example.digikala.Model.Message.Messages;

import java.util.List;

import io.reactivex.Single;

public class ProductDetailViewModel implements ProductDetailDataSoource{

    ProductDetailRepository productDetailRepository = new ProductDetailRepository();
    UserLoginInfo userLoginInfo = new UserLoginInfo(G.context);

    @Override
    public Single<List<DetailProduct>> get_DetailProduct_PDS(String id, String user) {
        return productDetailRepository.get_DetailProduct_PDS(id,user);
    }

    @Override
    public Single<Messages> addToBasket_PDS(String product_id, String email) {
        return productDetailRepository.addToBasket_PDS(product_id,email);
    }

    @Override
    public Single<Messages> getbasketcount_PDS(String email) {
        return productDetailRepository.getbasketcount_PDS(email);
    }

    public String getUserEmail(){
        return  userLoginInfo.get_UserEmail();
    }

}
