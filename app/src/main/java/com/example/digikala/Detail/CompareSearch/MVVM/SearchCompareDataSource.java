package com.example.digikala.Detail.CompareSearch.MVVM;

import com.example.digikala.Model.Product;

import java.util.List;

import io.reactivex.Single;

public interface SearchCompareDataSource {

    Single<List<Product>> get_SearchProductCompare(String search);
}
