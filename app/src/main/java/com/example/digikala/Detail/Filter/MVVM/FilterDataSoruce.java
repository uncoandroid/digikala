package com.example.digikala.Detail.Filter.MVVM;

import com.example.digikala.Model.Message.FilterMessage;
import com.example.digikala.Model.Product;

import org.json.JSONObject;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.Body;

public interface FilterDataSoruce {

    Single<List<Product>> getTabItem(String cat);

    Single<List<Product>> getSortProduct_FDS(String cat, int sort);

    Single<FilterMessage> sendFilterParam_FDS(@Body List<JSONObject> jsonObjects);
}
