package com.example.digikala.Detail.Fav;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.digikala.R;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.squareup.picasso.Picasso;

public class FavoriteBottomSheet extends BottomSheetDialogFragment {

    View view;
    ImageView img_logo, img_add;
    TextView txt_Count;
    LinearLayout lin_onSubmitTextClick;
    onAddFollderImageClick onAddFollderImageClick;
    OnSubmitLinClick onSubmitLinClick;

    public void SetonAddFollderImageClick(onAddFollderImageClick onAddFollderImageClick) {

        this.onAddFollderImageClick = onAddFollderImageClick;
    }

    public void SetonLinTextClick(OnSubmitLinClick onSubmitLinClick) {
        this.onSubmitLinClick = onSubmitLinClick;

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        if (view == null) {

            view = inflater.inflate(R.layout.favorite_bottom_sheet, null);
        }


        String url = getArguments().getString("url");
        int count = getArguments().getInt("count");
        setupViews();
        setFavImage(url);
        setFavCoutn(count);
        return view;


    }

    private void setFavCoutn(int coutn) {
        txt_Count.setText(coutn + "");
    }

    public void setFavImage(String url) {
      if (url !=null && !url.equals("")){
          Picasso.get().load(url).into(img_logo);
      }
    }

    private void setupViews() {
        img_logo = view.findViewById(R.id.img_logo);
        txt_Count = view.findViewById(R.id.txt_Count);
        img_add = view.findViewById(R.id.img_add);
        img_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onAddFollderImageClick.onClick();

            }
        });
        lin_onSubmitTextClick = view.findViewById(R.id.lin_onSubmitTextClick);
        lin_onSubmitTextClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSubmitLinClick.onClick();
            }
        });
    }

    public interface onAddFollderImageClick {
        void onClick();
    }

    public interface OnSubmitLinClick {
        void onClick();
    }
}
