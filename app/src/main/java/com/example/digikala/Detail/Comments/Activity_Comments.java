package com.example.digikala.Detail.Comments;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.digikala.Activity_Login.Activity_Sing_in;
import com.example.digikala.Activity_Main.MVVM.MainViewModel;
import com.example.digikala.Adapter.Adapter_Comments;
import com.example.digikala.Detail.AddComment.Activity_SetPointComment;
import com.example.digikala.Detail.Comments.MVVM.CommentViewModel;
import com.example.digikala.G;
import com.example.digikala.Model.Comment;
import com.example.digikala.Model.Message.Like_disLikeMessage;
import com.example.digikala.Model.Message.RegisterMessage;
import com.example.digikala.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static com.example.digikala.Activity_Main.MainActivity.txt_login_navigation;

public class Activity_Comments extends AppCompatActivity {


    Comment commentParam;
    ImageView img_close_comment;
    TextView txt_title;
    RecyclerView recyclerView;
    FloatingActionButton fabIcon;
    String b_title, b_id;
    CommentViewModel commentViewModel = new CommentViewModel();
    MainViewModel mainViewModel = new MainViewModel();
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    RegisterMessage registerMessage;
    Adapter_Comments adapter_comments;
    FrameLayout frameLayoutProgressFrame;
    public static final int LOGIN_REQUEST = 356;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comments);


        Bundle bundle = getIntent().getExtras();
        b_title = bundle.getString("title");
        b_id = bundle.getString("id");

        setupView();
        observeComment();
        checkUserEmail();

    }

    private void checkUserEmail() {
        String email = mainViewModel.getUserEmail();
        if (!email.equals("")) {
            txt_login_navigation.setText(email);
            txt_login_navigation.setBackgroundColor(Color.TRANSPARENT);
        }
    }

    private void observeComment() {
        commentViewModel.get_Comment_CVM(b_id).subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<List<Comment>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(final List<Comment> comments) {
                        if (comments.size() > 0) {


                            commentParam = comments.get(0);
                            frameLayoutProgressFrame.setVisibility(View.GONE);
                            adapter_comments = new Adapter_Comments(comments, new Adapter_Comments.OnLikeOrDisLike() {
                                @Override
                                public void OnLikeOrDisLike(final Comment comment, final String like_or_disLike) {
                                    commentViewModel.get_LikeOrDisLike(comment, like_or_disLike)
                                            .subscribeOn(Schedulers.newThread())
                                            .observeOn(AndroidSchedulers.mainThread())
                                            .subscribe(new SingleObserver<Like_disLikeMessage>() {
                                                @Override
                                                public void onSubscribe(Disposable d) {
                                                    compositeDisposable.add(d);
                                                }

                                                @Override
                                                public void onSuccess(Like_disLikeMessage like_disLikeMessage) {

                                                    if (like_disLikeMessage.getStatus().equals("خطای نا مشخص")) {

                                                    } else {
                                                        adapter_comments.Change_LikeORDisLikeCount(comment, like_or_disLike);
                                                    }
                                                }

                                                @Override
                                                public void onError(Throwable e) {

                                                }
                                            });
                                }
                            });


                            recyclerView.setAdapter(adapter_comments);

                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                });
    }

    private void setupView() {
        registerMessage = new RegisterMessage();
        frameLayoutProgressFrame = findViewById(R.id.frame_comment_progressFrame);
        img_close_comment = findViewById(R.id.img_close_comment);
        img_close_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        txt_title = findViewById(R.id.txt_title_comment);
        txt_title.setText(b_title);
        recyclerView = findViewById(R.id.rv_comment_productDetails);
        recyclerView.setLayoutManager(new LinearLayoutManager(G.context, LinearLayoutManager.VERTICAL, false));
        fabIcon = findViewById(R.id.fab_writeComment);
        fabIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (txt_login_navigation.getText().toString().equals("ورود   |   ثبت نام")) {

                    Intent intent = new Intent(G.context, Activity_Sing_in.class);
                    startActivityForResult(intent, LOGIN_REQUEST);
                } else {
                    Intent intent = new Intent(G.context, Activity_SetPointComment.class);
                    intent.putExtra("CommentParam", commentParam);
                    intent.putExtra("id", b_id);
                    startActivity(intent);
                }


            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == LOGIN_REQUEST && resultCode == RESULT_OK) {
            String email = data.getExtras().getString("email");
            txt_login_navigation.setText(email);
            txt_login_navigation.setBackgroundColor(Color.TRANSPARENT);
            mainViewModel.saveUserEmail(email);
        }

    }


    @Override
    protected void onResume() {
        super.onResume();
        checkUserEmail();
    }

    @Override
    protected void onDestroy() {
        compositeDisposable.dispose();
        super.onDestroy();
    }
}
