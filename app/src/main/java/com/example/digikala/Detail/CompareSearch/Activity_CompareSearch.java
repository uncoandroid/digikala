package com.example.digikala.Detail.CompareSearch;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.digikala.Adapter.Adapter_AddCompare;
import com.example.digikala.Adapter.Adapter_Product;
import com.example.digikala.Detail.Activity_ProductDetails;
import com.example.digikala.Detail.CompareSearch.MVVM.SearchCompareViewModel;
import com.example.digikala.G;
import com.example.digikala.Model.Product;
import com.example.digikala.R;

import java.util.List;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class Activity_CompareSearch extends AppCompatActivity {

    ProgressBar progress_compareSearch;
    ImageView img_close,img_clearEditText;
    EditText edtSearch;
    TextView txt_messageEmptyProduct;
    RecyclerView rv_compareSearchProduct;
    SearchCompareViewModel searchCompareViewModel = new SearchCompareViewModel();
    CompositeDisposable compositeDisposable = new CompositeDisposable();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compare_search);

        setupView();
    }

    private void setupView() {


        progress_compareSearch = findViewById(R.id.progress_compareSearch);

        rv_compareSearchProduct = findViewById(R.id.rv_compareSearchProduct);
        rv_compareSearchProduct.setLayoutManager(new LinearLayoutManager(G.context, LinearLayoutManager.VERTICAL, false));

        txt_messageEmptyProduct = findViewById(R.id.txt_messageEmptyProduct);
        img_close = findViewById(R.id.img_close);
        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        edtSearch = findViewById(R.id.edtSearch);
        img_clearEditText = findViewById(R.id.img_clearEditText);
        img_clearEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edtSearch.setText("");
            }
        });
        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                progress_compareSearch.setVisibility(View.VISIBLE);
            }

            @Override
            public void onTextChanged(final CharSequence s, int start, int before, int count) {
                searchCompareViewModel.getSearcCompareProduct(edtSearch.getText().toString())
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new SingleObserver<List<Product>>() {
                            @Override
                            public void onSubscribe(Disposable d) {
                                compositeDisposable.add(d);
                            }

                            @Override
                            public void onSuccess(List<Product> products) {
                                progress_compareSearch.setVisibility(View.GONE);
                                img_clearEditText.setVisibility(View.VISIBLE);
                                if (edtSearch.getText().toString().equals("")) {
                                    products.clear();
                                    img_clearEditText.setVisibility(View.INVISIBLE);
                                }
                                if (String.valueOf(s).equals("")){
                                    txt_messageEmptyProduct.setVisibility(View.VISIBLE);
                                    rv_compareSearchProduct.setVisibility(View.GONE);
                                    img_clearEditText.setVisibility(View.INVISIBLE);
                                }else{
                                    img_clearEditText.setVisibility(View.VISIBLE);
                                    rv_compareSearchProduct.setVisibility(View.VISIBLE);
                                    rv_compareSearchProduct.setAdapter(new Adapter_AddCompare(products, new Adapter_AddCompare.OnSearchedItemClickListener() {
                                        @Override
                                        public void onClickListener(String properties, String image, String id, String title) {
                                            Intent intent = new Intent();
                                            setResult(RESULT_OK,intent);
                                            intent.putExtra("properties",properties);
                                            intent.putExtra("title",title);
                                            intent.putExtra("image",image);
                                            finish();
                                        }
                                    }));

                                }

                            }

                            @Override
                            public void onError(Throwable e) {

                            }
                        });
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    @Override
    protected void onDestroy() {
        compositeDisposable.dispose();
        super.onDestroy();
    }
}
