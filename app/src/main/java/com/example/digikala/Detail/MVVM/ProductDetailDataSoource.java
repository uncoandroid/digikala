package com.example.digikala.Detail.MVVM;

import com.example.digikala.Model.DetailProduct;
import com.example.digikala.Model.Message.Messages;

import java.util.List;

import io.reactivex.Single;

public interface ProductDetailDataSoource {

    Single<List<DetailProduct>> get_DetailProduct_PDS(String id, String user);

    Single<Messages> addToBasket_PDS(String product_id, String email);

    Single<Messages> getbasketcount_PDS(String email);
}
