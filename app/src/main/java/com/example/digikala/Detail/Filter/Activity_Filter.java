
package com.example.digikala.Detail.Filter;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.example.digikala.Adapter.Adapter_Filter;
import com.example.digikala.Detail.Filter.MVVM.FilterViewModel;
import com.example.digikala.G;
import com.example.digikala.Model.Product;
import com.example.digikala.R;

import org.json.JSONObject;

import java.util.List;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class Activity_Filter extends AppCompatActivity {

    String b_id, b_title, b_cat;
    RecyclerView rv_filterProduct;
    LinearLayout lin_sort_click, lin_filter_click;
    Adapter_Filter adapter_filter;
    String filterParams;

    FilterViewModel filterViewModel = new FilterViewModel();
    CompositeDisposable compositeDisposable = new CompositeDisposable();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity__filter);

        Bundle bundle = getIntent().getExtras();

        b_id = bundle.getString("id");
        b_title = bundle.getString("title");
        b_cat = bundle.getString("cat");


        setupViews();
        observeForSort();
    }

    private void observeForSort() {
        filterViewModel.getTabItem(b_cat)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<List<Product>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(List<Product> products) {
                        rv_filterProduct.setAdapter(new Adapter_Filter(products));
                        filterParams = products.get(0).getFilter_item();
                        YoYo.with(Techniques.FadeIn)
                                .duration(1000)
                                .repeat(0)
                                .playOn(rv_filterProduct);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                });
    }

    private void setupViews() {
        ImageView img_close_filter = findViewById(R.id.img_close_filter);
        img_close_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        rv_filterProduct = findViewById(R.id.rv_filterProduct);
        rv_filterProduct.setLayoutManager(new LinearLayoutManager(G.context, RecyclerView.VERTICAL, false));

        lin_sort_click = findViewById(R.id.lin_sort_click);
        lin_sort_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final SortDialog sortDialog = new SortDialog();
                sortDialog.setOnDialogItemClick(new SortDialog.onDialogItemClick() {
                    @Override
                    public void onClick(int sort) {
                        filterViewModel.getSortProduct_FDS(b_cat,sort)
                                .subscribeOn(Schedulers.newThread())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(new SingleObserver<List<Product>>() {
                                    @Override
                                    public void onSubscribe(Disposable d) {
                                        compositeDisposable.add(d);
                                    }

                                    @Override
                                    public void onSuccess(List<Product> products) {
                                        adapter_filter = new Adapter_Filter(products);

                                        YoYo.with(Techniques.FadeIn)
                                                .duration(1000)
                                                .repeat(0)
                                                .playOn(rv_filterProduct);
                                        adapter_filter.notifyDataSetChanged();
                                        rv_filterProduct.setAdapter(adapter_filter);


                                    }

                                    @Override
                                    public void onError(Throwable e) {

                                    }
                                });
                    }
                });
                sortDialog.show(getSupportFragmentManager(), null);
            }
        });
        lin_filter_click = findViewById(R.id.lin_filter_click);
        lin_filter_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(G.context, Activity_filter_details.class);
                intent.putExtra("filterParams",filterParams);
                startActivity(intent);

            }
        });


    }

    public void filterProducts_method(List<JSONObject> jsonObjects){

    }
    @Override
    protected void onDestroy() {
        compositeDisposable.dispose();
        super.onDestroy();
    }
}
