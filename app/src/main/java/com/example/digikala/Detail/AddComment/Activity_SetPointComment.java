package com.example.digikala.Detail.AddComment;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.digikala.Adapter.Adapter_AddComment;
import com.example.digikala.Detail.AddComment.MVVM.AddCommentViewModel;
import com.example.digikala.Detail.WriteComment.Activity_WriteComment;
import com.example.digikala.G;
import com.example.digikala.Model.Comment;
import com.example.digikala.Model.Message.CommentMessage;
import com.example.digikala.Model.RatingModel;
import com.example.digikala.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static com.example.digikala.Activity_Main.MainActivity.txt_login_navigation;

public class Activity_SetPointComment extends AppCompatActivity {

    ImageView img_close_addComment;
    Comment comment;
    List<RatingModel> ratingModelList;
    AddCommentViewModel addCommentViewModel = new AddCommentViewModel();
    RecyclerView rv_addComment_List;
    Button btn_writeComment, btn_setPoint;
    Adapter_AddComment adapter_set_point_comment;
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    String b_id;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_point_comment);

        comment = getIntent().getParcelableExtra("CommentParam");
        Bundle bundle = getIntent().getExtras();
        b_id = bundle.getString("id");


        setupViews();
        getParamComment();


    }


    private void getParamComment() {
        String param = comment.getParam();
        try {
            JSONArray jsonArray = new JSONArray(param);
            for (int i = 0; i < jsonArray.length(); i++) {
                RatingModel ratingModel = new RatingModel();
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                String title = jsonObject.getString("title");
                String value = jsonObject.getString("value");
                ratingModel.setTitle(title);
                ratingModel.setValue(value);
                ratingModelList.add(ratingModel);

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        adapter_set_point_comment = new Adapter_AddComment(ratingModelList);
        rv_addComment_List.setAdapter(adapter_set_point_comment);

    }

    private void observeSetPoint() {
        addCommentViewModel.sendPoint_AVD(adapter_set_point_comment.getRatingModelList())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<CommentMessage>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(CommentMessage commentMessage) {
                        finish();
                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                });
    }


    private void setupViews() {
        ratingModelList = new ArrayList<>();
        btn_setPoint = findViewById(R.id.btn_setPoint);
        btn_setPoint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                observeSetPoint();

            }
        });

        btn_writeComment = findViewById(R.id.btn_writeComment);
        btn_writeComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(G.context, Activity_WriteComment.class);
                startActivityForResult(intent, 100);
            }
        });
        rv_addComment_List = findViewById(R.id.rv_addComment_List);
        rv_addComment_List.setLayoutManager(new LinearLayoutManager(G.context, LinearLayoutManager.VERTICAL, false));

        img_close_addComment = findViewById(R.id.img_close_addComment);
        img_close_addComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100 && resultCode == RESULT_OK) {
            String title = data.getExtras().getString("title");
            String positive = data.getExtras().getString("positive");
            String negative = data.getExtras().getString("negative");
            String passage = data.getExtras().getString("passage");
            String user = txt_login_navigation.getText().toString();
            String idproduct = b_id;
            addCommentViewModel.WriteComment_AVD(title, positive, negative, passage, user, idproduct)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new SingleObserver<CommentMessage>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            compositeDisposable.add(d);
                        }

                        @Override
                        public void onSuccess(CommentMessage commentMessage) {
                            Toast.makeText(Activity_SetPointComment.this, "نظر رشما با موفقیت ثبت شد", Toast.LENGTH_SHORT).show();
                            finish();
                        }

                        @Override
                        public void onError(Throwable e) {

                        }
                    });
        }
    }

    @Override
    protected void onDestroy() {
        compositeDisposable.clear();
        super.onDestroy();
    }
}
