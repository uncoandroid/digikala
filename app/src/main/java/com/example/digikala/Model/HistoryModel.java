
package com.example.digikala.Model;


import com.google.gson.annotations.SerializedName;


public class HistoryModel {

    @SerializedName("date")
    private String date;
    @SerializedName("id")
    private String id;
    @SerializedName("price")
    private String price;
    @SerializedName("product_id")
    private String product_id;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }
}
