
package com.example.digikala.Model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.SerializedName;


@SuppressWarnings("unused")
public class FilterItem  {

    @SerializedName("title")
    private String Title;
    @SerializedName("values")
    private List<Value> Values;

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public List<Value> getValues() {
        return Values;
    }

    public void setValues(List<Value> values) {
        Values = values;
    }
}
