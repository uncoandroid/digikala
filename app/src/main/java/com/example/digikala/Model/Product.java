package com.example.digikala.Model;

import com.google.gson.annotations.SerializedName;

public class Product {

    @SerializedName("id")
    private String id;

    @SerializedName("title")
    private String title;

    @SerializedName("price")
    private String price;

    @SerializedName("pprice")
    private String pprice;

    @SerializedName("image")
    private String image;

    @SerializedName("properties")
    private String properties;

    @SerializedName("filter_item")
    private String filter_item;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPprice() {
        return pprice;
    }

    public void setPprice(String pprice) {
        this.pprice = pprice;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getProperties() {
        return properties;
    }

    public void setProperties(String properties) {
        this.properties = properties;
    }

    public String getFilter_item() {
        return filter_item;
    }

    public void setFilter_item(String filter_item) {
        this.filter_item = filter_item;
    }
}