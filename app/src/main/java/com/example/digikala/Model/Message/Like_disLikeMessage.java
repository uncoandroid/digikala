package com.example.digikala.Model.Message;

import com.google.gson.annotations.SerializedName;

public class Like_disLikeMessage {

    @SerializedName("id")
    private String id;

    @SerializedName("message")
    private String message;

    @SerializedName("status")
    private String status;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
