package com.example.digikala.Model;

import com.google.gson.annotations.SerializedName;

public class MyTimer {

    @SerializedName("hour")
    private int hour;

    @SerializedName("min")
    private int min;

    @SerializedName("sec")
    private int sec;

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public int getSec() {
        return sec;
    }

    public void setSec(int sec) {
        this.sec = sec;
    }
}
