
package com.example.digikala.Model;

import java.util.List;

import com.google.gson.annotations.SerializedName;


public class DetailProduct {


    @SerializedName("id")
    private String Id;

    @SerializedName("colors")
    private String Colors;

    @SerializedName("comments")
    private List<Comment> Comments;

    @SerializedName("garantee")
    private String Garantee;


    @SerializedName("image")
    private String Image;

    @SerializedName("introduction")
    private String Introduction;

    @SerializedName("pprice")
    private String Pprice;

    @SerializedName("price")
    private String Price;

    @SerializedName("properties")
    private String Properties;

    @SerializedName("rating")
    private String Rating;

    @SerializedName("rating_item")
    private String RatingItem;

    @SerializedName("title")
    private String Title;

    @SerializedName("weight")
    private String Weight;

    @SerializedName("fav")
    private List<DetailFavorite> favoriteList;


    public String getColors() {
        return Colors;
    }

    public void setColors(String colors) {
        Colors = colors;
    }

    public List<Comment> getComments() {
        return Comments;
    }

    public void setComments(List<Comment> comments) {
        Comments = comments;
    }

    public String getGarantee() {
        return Garantee;
    }

    public void setGarantee(String garantee) {
        Garantee = garantee;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public String getIntroduction() {
        return Introduction;
    }

    public void setIntroduction(String introduction) {
        Introduction = introduction;
    }

    public String getPprice() {
        return Pprice;
    }

    public void setPprice(String pprice) {
        Pprice = pprice;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }

    public String getProperties() {
        return Properties;
    }

    public void setProperties(String properties) {
        Properties = properties;
    }

    public String getRating() {
        return Rating;
    }

    public void setRating(String rating) {
        Rating = rating;
    }

    public String getRatingItem() {
        return RatingItem;
    }

    public void setRatingItem(String ratingItem) {
        RatingItem = ratingItem;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getWeight() {
        return Weight;
    }

    public void setWeight(String weight) {
        Weight = weight;
    }

    public List<DetailFavorite> getFavoriteList() {
        return favoriteList;
    }

    public void setFavoriteList(List<DetailFavorite> favoriteList) {
        this.favoriteList = favoriteList;
    }
}
