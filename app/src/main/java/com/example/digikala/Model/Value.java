
package com.example.digikala.Model;

import com.google.gson.annotations.SerializedName;


public class Value {

    @SerializedName("title")
    private String Title;

    private boolean isChecked;


    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }
}
