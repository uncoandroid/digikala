
package com.example.digikala.Model;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


public class Comment implements Parcelable {

    @SerializedName("dislikecount")
    private String Dislikecount;
    @SerializedName("id")
    private String Id;
    @SerializedName("likecount")
    private String Likecount;
    @SerializedName("negative")
    private String Negative;
    @SerializedName("param")
    private String Param;
    @SerializedName("passage")
    private String Passage;
    @SerializedName("positive")
    private String Positive;
    @SerializedName("suggest")
    private String Suggest;
    @SerializedName("title")
    private String Title;
    @SerializedName("user")
    private String User;
    @SerializedName("idproduct")
    private String Idproduct;


    public String getDislikecount() {
        return Dislikecount;
    }

    public void setDislikecount(String dislikecount) {
        Dislikecount = dislikecount;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getLikecount() {
        return Likecount;
    }

    public void setLikecount(String likecount) {
        Likecount = likecount;
    }

    public String getNegative() {
        return Negative;
    }

    public void setNegative(String negative) {
        Negative = negative;
    }

    public String getParam() {
        return Param;
    }

    public void setParam(String param) {
        Param = param;
    }

    public String getPassage() {
        return Passage;
    }

    public void setPassage(String passage) {
        Passage = passage;
    }

    public String getPositive() {
        return Positive;
    }

    public void setPositive(String positive) {
        Positive = positive;
    }

    public String getSuggest() {
        return Suggest;
    }

    public void setSuggest(String suggest) {
        Suggest = suggest;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getUser() {
        return User;
    }

    public void setUser(String user) {
        User = user;
    }

    public String getIdproduct() {
        return Idproduct;
    }

    public void setIdproduct(String idproduct) {
        Idproduct = idproduct;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.Dislikecount);
        dest.writeString(this.Id);
        dest.writeString(this.Likecount);
        dest.writeString(this.Negative);
        dest.writeString(this.Param);
        dest.writeString(this.Passage);
        dest.writeString(this.Positive);
        dest.writeString(this.Suggest);
        dest.writeString(this.Title);
        dest.writeString(this.User);
        dest.writeString(this.Idproduct);
    }

    public Comment() {
    }

    protected Comment(Parcel in) {
        this.Dislikecount = in.readString();
        this.Id = in.readString();
        this.Likecount = in.readString();
        this.Negative = in.readString();
        this.Param = in.readString();
        this.Passage = in.readString();
        this.Positive = in.readString();
        this.Suggest = in.readString();
        this.Title = in.readString();
        this.User = in.readString();
        this.Idproduct = in.readString();
    }

    public static final Parcelable.Creator<Comment> CREATOR = new Parcelable.Creator<Comment>() {
        @Override
        public Comment createFromParcel(Parcel source) {
            return new Comment(source);
        }

        @Override
        public Comment[] newArray(int size) {
            return new Comment[size];
        }
    };
}