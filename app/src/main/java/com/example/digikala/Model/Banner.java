package com.example.digikala.Model;

import com.google.gson.annotations.SerializedName;

public class Banner {

    @SerializedName("id")
    private String id;


    @SerializedName("image")
    private String image;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}