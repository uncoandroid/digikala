package com.example.digikala.Search.MVVM;

import com.example.digikala.Model.Product;
import com.example.digikala.Retrofit.ApiProvider;
import com.example.digikala.Retrofit.ApiService;

import java.util.List;

import io.reactivex.Single;

public class Search_ApiService  implements Search_DataSource {
    ApiService apiService = ApiProvider.apiProvider();
    @Override
    public Single<List<Product>> getProduct_Searcj_SDS(String search) {
        return apiService.get_SearchProduct_php(search);
    }
}
