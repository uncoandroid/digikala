package com.example.digikala.Search;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.digikala.Adapter.Adapter_AddCompare;
import com.example.digikala.Detail.Activity_ProductDetails;
import com.example.digikala.G;
import com.example.digikala.Model.Product;
import com.example.digikala.R;
import com.example.digikala.Search.MVVM.Search_ViewModel;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Scheduler;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class Activity_Search extends AppCompatActivity {

    ImageView img_close, img_microphone;
    EditText edt_search;
    RecyclerView rv_search_mainActivity;
    Search_ViewModel search_viewModel = new Search_ViewModel();
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    ProgressBar progress_search;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity__search);

        setupView();

    }


    private void setupView() {
        progress_search = findViewById(R.id.progress_compareSearch);
        img_close = findViewById(R.id.img_close);
        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        img_microphone = findViewById(R.id.img_microphone);
        img_microphone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "fa");
                intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "نام محصول را بگویید");
                try {
                    startActivityForResult(intent, 100);
                } catch (ActivityNotFoundException e) {
                    Toast.makeText(G.context, "متاسفانه گوشی شما از این قابلیت پشتیبانی نمی کند", Toast.LENGTH_SHORT).show();
                }
            }
        });
        rv_search_mainActivity = findViewById(R.id.rv_search_mainActivity);
        rv_search_mainActivity.setLayoutManager(new LinearLayoutManager(G.context));
        edt_search = findViewById(R.id.edt_search_mainActivity);
        edt_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(final CharSequence s, int start, int count, int after) {
                progress_search.setVisibility(View.VISIBLE);
            }

            @Override
            public void onTextChanged(final CharSequence s, int start, int before, int count) {
                search_viewModel.getProduct_Searcj_SDS(s.toString())
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new SingleObserver<List<Product>>() {
                            @Override
                            public void onSubscribe(Disposable d) {
                                compositeDisposable.add(d);
                            }

                            @Override
                            public void onSuccess(final List<Product> products) {
                                progress_search.setVisibility(View.GONE);
                                if (edt_search.getText().toString().equals("")) {
                                    products.clear();
                                }

                                if (String.valueOf(s).equals("")){
                                    rv_search_mainActivity.setVisibility(View.GONE);
                                }else{

                                    rv_search_mainActivity.setVisibility(View.VISIBLE);
                                    rv_search_mainActivity.setAdapter(new Adapter_AddCompare(products, new Adapter_AddCompare.OnSearchedItemClickListener() {
                                        @Override
                                        public void onClickListener(String properties, String image, String id, String title) {
                                            Intent intent = new Intent(G.context, Activity_ProductDetails.class);
                                            intent.putExtra("id",id);
                                            startActivity(intent);
                                            finish();
                                        }
                                    }));

                                }

                            }


                            @Override
                            public void onError(Throwable e) {

                            }
                        });

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


    }

    @Override
    protected void onDestroy() {
        compositeDisposable.dispose();
        super.onDestroy();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100 && resultCode == RESULT_OK) {
            ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            edt_search.setText(result.get(0));
        }
    }

}
