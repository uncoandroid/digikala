package com.example.digikala.Search.MVVM;

import com.example.digikala.Model.Product;

import java.util.List;

import io.reactivex.Single;

public class Search_Repository  implements Search_DataSource{
    Search_ApiService search_apiService =  new Search_ApiService();
    @Override
    public Single<List<Product>> getProduct_Searcj_SDS(String search) {
        return search_apiService.getProduct_Searcj_SDS(search);
    }
}
