package com.example.digikala.Search.MVVM;

import com.example.digikala.Model.Product;

import java.util.List;

import io.reactivex.Single;

public interface Search_DataSource {
    Single<List<Product>> getProduct_Searcj_SDS(String search);
}
