package com.example.digikala.TabLayoutCategory.MVVM;

import com.example.digikala.Model.Product;

import java.util.List;

import io.reactivex.Single;

public interface Category_DataSource {

    Single<List<Product>> getCategoryTabItem(String cat);
}
