package com.example.digikala.TabLayoutCategory.MVVM;

import com.example.digikala.Model.Product;
import com.example.digikala.Retrofit.ApiProvider;
import com.example.digikala.Retrofit.ApiService;

import java.util.List;

import io.reactivex.Single;

public class CategoryApiService implements Category_DataSource {
    public ApiService apiService = ApiProvider.apiProvider();


    @Override
    public Single<List<Product>> getCategoryTabItem(String cat) {
        return apiService.get_CategoryTabItem_php(cat);
    }
}
