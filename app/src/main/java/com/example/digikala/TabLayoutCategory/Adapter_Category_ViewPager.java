package com.example.digikala.TabLayoutCategory;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;



import java.util.ArrayList;
import java.util.List;


public class Adapter_Category_ViewPager extends FragmentPagerAdapter {


    List<String> titles;

    public Adapter_Category_ViewPager(FragmentManager fm) {
        super(fm);
        titles = new ArrayList<>();
    }


    @Override
    public Fragment getItem(int i) {
        return Fragment_Category.newInstance(titles.get(i));
    }

    @Override
    public int getCount() {
        return titles.size();
    }

    public void addFramgent(String tilte){
        titles.add(tilte);
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return titles.get(position);
    }
}
