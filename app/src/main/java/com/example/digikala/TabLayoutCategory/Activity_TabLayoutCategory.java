package com.example.digikala.TabLayoutCategory;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.example.digikala.Model.Cat;
import com.example.digikala.R;
import com.google.android.material.tabs.TabLayout;

import java.util.List;

public class Activity_TabLayoutCategory extends AppCompatActivity {

    ImageView imgClose;
    TabLayout tabLayout;
    ViewPager viewPager;
    List<Cat> catList;
    int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab_layout_category);

        catList = getIntent().getParcelableArrayListExtra("cats");
        position = getIntent().getExtras().getInt("position");

        setupView();

    }


    private void setupView() {
        imgClose = findViewById(R.id.img_close);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        tabLayout = findViewById(R.id.tl_categoryTabLayout);
        viewPager = findViewById(R.id.vp_category_viewPager);
        tabLayout.setupWithViewPager(viewPager);
        Adapter_Category_ViewPager adapter = new Adapter_Category_ViewPager(getSupportFragmentManager());
        for (int i = catList.size() - 1; i >= 0; i--) {
            adapter.addFramgent(catList.get(i).getTitle());
        }

        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem((catList.size() - 1) - position);

    }
}
