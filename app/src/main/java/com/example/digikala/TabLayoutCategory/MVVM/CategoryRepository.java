package com.example.digikala.TabLayoutCategory.MVVM;

import com.example.digikala.Model.Product;

import java.util.List;

import io.reactivex.Single;

public class CategoryRepository  implements  Category_DataSource{
    CategoryApiService categoryApiService = new CategoryApiService();


    @Override
    public Single<List<Product>> getCategoryTabItem(String cat) {
        return  categoryApiService.getCategoryTabItem(cat);
    }
}
