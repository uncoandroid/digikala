package com.example.digikala.TabLayoutCategory.MVVM;

import com.example.digikala.Model.Product;

import java.util.List;

import io.reactivex.Single;

public class CategoryViewModel implements  Category_DataSource{

    CategoryRepository categoryRepository = new CategoryRepository();


    @Override
    public Single<List<Product>> getCategoryTabItem(String cat) {
        return categoryRepository.getCategoryTabItem(cat);
    }
}
