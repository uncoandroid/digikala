package com.example.digikala.TabLayoutCategory;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.digikala.Adapter.CatAdapter;
import com.example.digikala.Detail.Filter.Activity_Filter;
import com.example.digikala.G;
import com.example.digikala.Model.Product;
import com.example.digikala.R;
import com.example.digikala.TabLayoutCategory.MVVM.CategoryViewModel;

import java.util.List;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class Fragment_Category  extends Fragment {
    View view;
    String cat;
    RecyclerView recyclerView;
    CategoryViewModel viewModel=new CategoryViewModel();
    CompositeDisposable compositeDisposable=new CompositeDisposable();
    public static Fragment_Category newInstance(String title) {
        Bundle args = new Bundle();
        args.putString("title",title);
        Fragment_Category fragment = new Fragment_Category();
        fragment.setArguments(args);
        return fragment;
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_category, container, false);
        }

        setupViews();
        viewModel.getCategoryTabItem(cat)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<List<Product>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(List<Product> products) {

                        recyclerView.setAdapter(new CatAdapter(products, new CatAdapter.OnItemClickListener() {
                            @Override
                            public void onItemClick(String id) {
                                Intent intent=new Intent(getActivity(), Activity_Filter.class);
                                intent.putExtra("id",id);
                                intent.putExtra("cat",cat);
                                startActivity(intent);
                            }
                        }));

                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.i("LOG", "onError: "+e.toString());
                    }
                });
        return view;
    }



    private void setupViews() {
        cat=getArguments().getString("title");
        recyclerView=view.findViewById(R.id.rv_catFragment);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        Log.i("LOG",cat);
    }

    @Override
    public void onDestroy() {
        compositeDisposable.clear();
        super.onDestroy();
    }


}
