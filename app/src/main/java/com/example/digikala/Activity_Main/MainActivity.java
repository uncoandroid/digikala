package com.example.digikala.Activity_Main;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.example.digikala.Activity_Main.MVVM.MainViewModel;
import com.example.digikala.Adapter.Adapter_Category;
import com.example.digikala.Adapter.Adapter_MainSlider;
import com.example.digikala.Adapter.Adapter_Product;
import com.example.digikala.Basket.Activity_BasketList;
import com.example.digikala.Detail.Activity_ProductDetails;
import com.example.digikala.G;
import com.example.digikala.Activity_Login.Activity_Sing_in;
import com.example.digikala.Model.Banner;
import com.example.digikala.Model.Cat;
import com.example.digikala.Model.Message.Messages;
import com.example.digikala.Model.MyTimer;
import com.example.digikala.Model.Product;
import com.example.digikala.Model.Carousel;
import com.example.digikala.Profile.Activity_Profile;
import com.example.digikala.R;
import com.example.digikala.Search.Activity_Search;
import com.example.digikala.TabLayoutCategory.Activity_TabLayoutCategory;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import ss.com.bannerslider.Slider;

import static com.example.digikala.Adapter.Adapter_Category.catListt;


public class MainActivity extends AppCompatActivity {
    String User_email;
    public static DrawerLayout drawerLayout;
    Timer timer;
    ImageView img_hamburger_menu;
    ImageView img_search;
    ImageView img_shopping;
    ImageView img_banner1, img_banner2, img_banner3, img_banner4;
    RecyclerView rv_superPRODUCTS, rv_newestPRODUCTS, rv_categoryList;
    MainViewModel mainViewModel = new MainViewModel();
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    TextView txtHour, txtMin, txtSec;
    TextView txt_basketCount, txt_count_navigation;
    RelativeLayout relative_basketClick;
    Slider slider;
    public static TextView txt_login_navigation;
    public static final int LOGIN_REQUEST = 356;
    LinearLayout lin_Navigation_home, lin_navigation_exit, lin_category_list_navigation, lin_Basketcount_navigation;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.z_drawer);

        setupViews();
        observeForCategoryList();
        observeForProduct();
        observeForNewestProduct();
        observeForBanner();
        observeForSlider();
        observeForTimer();
        checkUserEmail();


    }

    private void observeForBasketCount() {
        mainViewModel.get_basketCount_MVM(mainViewModel.getUserEmail())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<Messages>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(Messages messages) {
                        if (!messages.getMessage().equals("0")) {
                            txt_basketCount.setVisibility(View.VISIBLE);
                            txt_basketCount.setText(messages.getMessage());

                            txt_count_navigation.setText(messages.getMessage());
                            txt_count_navigation.setVisibility(View.VISIBLE);

                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                });
    }

    private void observeForCategoryList() {
        mainViewModel.get_CategoryListMVM()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<List<Cat>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(final List<Cat> cats) {
                        rv_categoryList.setAdapter(new Adapter_Category(cats, new Adapter_Category.OnCategoriesClick() {
                            @Override
                            public void OnClick(int position, String title) {
                                Intent intent = new Intent(G.context, Activity_TabLayoutCategory.class);
                                intent.putExtra("position", position);
                                intent.putExtra("title", title);
                                intent.putParcelableArrayListExtra("cats", (ArrayList<? extends Parcelable>) cats);
                                startActivity(intent);
                                Log.i("LOG", "catList: "+cats);
                            }

                        }));
                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                });
    }

    private void checkUserEmail() {
        String email = mainViewModel.getUserEmail();
        if (!email.equals("")) {
            txt_login_navigation.setText(email);
            txt_login_navigation.setBackgroundColor(Color.TRANSPARENT);
            observeForBasketCount();
        } else {
            txt_login_navigation.setText("ورود   |   ثبت نام");
            txt_login_navigation.setBackground(getResources().getDrawable(R.drawable.shape_txt_login_in_navigation));
        }
    }

    private void observeForTimer() {
        mainViewModel.get_timer_MVM()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<MyTimer>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(final MyTimer myTimer) {

                        if (myTimer.getHour() < 10) {
                            txtHour.setText("0" + myTimer.getHour());
                        } else {
                            txtHour.setText(myTimer.getHour() + "");
                        }
                        if (myTimer.getMin() < 10) {
                            txtMin.setText("0" + myTimer.getMin());
                        } else {
                            txtMin.setText(myTimer.getMin() + "");
                        }
                        if (myTimer.getSec() < 10) {
                            txtSec.setText("0" + myTimer.getSec());
                        } else {
                            txtSec.setText(myTimer.getSec() + "");
                        }


                        timer = new Timer();
                        timer.scheduleAtFixedRate(new TimerTask() {
                            @Override
                            public void run() {

                                if (myTimer.getSec() != 0) {
                                    myTimer.setSec(myTimer.getSec() - 1);
                                    if (myTimer.getSec() < 10) {
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                txtSec.setText("0" + myTimer.getSec());
                                            }
                                        });

                                    } else {
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                txtSec.setText(myTimer.getSec() + "");
                                            }
                                        });

                                    }
                                } else {
                                    if (myTimer.getMin() != 0) {
                                        myTimer.setMin(myTimer.getMin() - 1);
                                        myTimer.setSec(59);

                                        if (myTimer.getMin() < 10) {
                                            myTimer.setMin(myTimer.getMin() - 1);
                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    txtMin.setText("0" + myTimer.getMin());
                                                    txtSec.setText(myTimer.getSec() + "");
                                                }
                                            });

                                        } else {
                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    txtMin.setText(myTimer.getMin() + "");
                                                    txtSec.setText(myTimer.getSec() + "");
                                                }
                                            });

                                        }

                                    } else {
                                        myTimer.setMin(59);
                                        myTimer.setSec(59);
                                        myTimer.setHour(myTimer.getHour() - 1);

                                        if (myTimer.getHour() < 10) {
                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    txtHour.setText("0" + myTimer.getHour());
                                                    txtMin.setText(myTimer.getMin() + "");
                                                    txtSec.setText(myTimer.getSec() + "");
                                                }
                                            });

                                        } else {
                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    txtHour.setText(myTimer.getHour() + "");
                                                    txtMin.setText(myTimer.getMin() + "");
                                                    txtSec.setText(myTimer.getSec() + "");
                                                }
                                            });

                                        }
                                    }

                                }
                            }
                        }, 0, 1000);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                });
    }

    private void observeForNewestProduct() {
        mainViewModel.get_NewestProdct_MVM()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<List<Product>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(List<Product> products) {

                        rv_newestPRODUCTS.setAdapter(new Adapter_Product(products, new Adapter_Product.onProductClick() {
                            @Override
                            public void onClick(String id, String title, String image) {
                                Intent intent = new Intent(G.context, Activity_ProductDetails.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.putExtra("id", id);
                                intent.putExtra("title", title);
                                intent.putExtra("image", image);
                                startActivity(intent);
                            }
                        }));


                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                });
    }

    private void observeForSlider() {
        mainViewModel.get_slider_MVM().subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<List<Carousel>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(List<Carousel> sliders) {

                        List<Carousel> sliderList = new ArrayList<>();
                        for (int i = 0; i < sliders.size(); i++) {
                            sliderList.add(sliders.get(i));

                            slider.setAdapter(new Adapter_MainSlider(sliderList));
                        }

                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                });
    }

    private void observeForBanner() {
        mainViewModel.get_Banner_MVM().subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<List<Banner>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(List<Banner> banners) {
                        List<Banner> bannerList = new ArrayList<>();
                        for (int i = 0; i < banners.size(); i++) {
                            bannerList.add(banners.get(i));
                        }
                        Picasso.get().load(bannerList.get(0).getImage()).into(img_banner1);
                        Picasso.get().load(bannerList.get(1).getImage()).into(img_banner2);
                        Picasso.get().load(bannerList.get(2).getImage()).into(img_banner3);
                        Picasso.get().load(bannerList.get(3).getImage()).into(img_banner4);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                });
    }

    private void observeForProduct() {
        mainViewModel.get_SuperProduct_MVM().subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<List<Product>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);

                    }

                    @Override
                    public void onSuccess(final List<Product> products) {

                        rv_superPRODUCTS.setAdapter(new Adapter_Product(products, new Adapter_Product.onProductClick() {
                            @Override
                            public void onClick(String id, String title, String image) {
                                Intent intent = new Intent(G.context, Activity_ProductDetails.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.putExtra("id", id);
                                intent.putExtra("title", title);
                                intent.putExtra("image", image);
                                intent.putExtra("email", User_email);
                                startActivity(intent);
                            }
                        }));


                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                });
    }

    private void setupViews() {
        txt_basketCount = findViewById(R.id.txt_basketCount_mainactivity);
        lin_Basketcount_navigation = findViewById(R.id.lin_Basketcount_navigation);
        lin_Basketcount_navigation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mainViewModel.getUserEmail().equals("")) {
                    Intent intent = new Intent(G.context, Activity_Sing_in.class);
                    startActivityForResult(intent, LOGIN_REQUEST);
                } else {
                    Intent intent = new Intent(G.context, Activity_BasketList.class);
                    intent.putExtra("basketCount", txt_basketCount.getText().toString());
                    startActivity(intent);
                }
            }
        });
        txt_count_navigation = findViewById(R.id.txt_count_navigation);
        if (!mainViewModel.getUserEmail().equals("")) {

            txt_count_navigation.setVisibility(View.VISIBLE);
            txt_count_navigation.setText(txt_basketCount.getText().toString());
        } else {
            txt_count_navigation.setVisibility(View.GONE);
        }
        relative_basketClick = findViewById(R.id.relative_basketClick);
        relative_basketClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mainViewModel.getUserEmail().equals("")) {
                    Intent intent = new Intent(G.context, Activity_Sing_in.class);
                    startActivityForResult(intent, LOGIN_REQUEST);
                } else {
                    Intent intent = new Intent(G.context, Activity_BasketList.class);
                    intent.putExtra("basketCount", txt_basketCount.getText().toString());
                    startActivity(intent);
                }
            }
        });
        lin_category_list_navigation = findViewById(R.id.lin_category_list_navigation);
        lin_category_list_navigation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(G.context, Activity_TabLayoutCategory.class);
                intent.putParcelableArrayListExtra("cats", (ArrayList<? extends Parcelable>) catListt);
                startActivity(intent);
            }
        });
        lin_navigation_exit = findViewById(R.id.lin_navigation_exit);
        lin_navigation_exit = findViewById(R.id.lin_navigation_exit);
        lin_navigation_exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        lin_Navigation_home = findViewById(R.id.lin_navigation_home);
        lin_Navigation_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.closeDrawer(Gravity.RIGHT);
            }
        });
        txt_login_navigation = findViewById(R.id.txt_login_navigation);
        txt_login_navigation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (txt_login_navigation.getText().toString().equals("ورود   |   ثبت نام")) {

                    Intent intent = new Intent(G.context, Activity_Sing_in.class);
                    startActivityForResult(intent, LOGIN_REQUEST);
                } else {
                    Intent intent = new Intent(G.context, Activity_Profile.class);
                    startActivity(intent);
                }

            }
        });

        txtHour = findViewById(R.id.txt_hour_mainActivity);
        txtMin = findViewById(R.id.txt_min_mainActivity);
        txtSec = findViewById(R.id.txt_sec_mainActivity);

        slider = findViewById(R.id.main_slider);
        Slider.init(new PicassoImageLoadingService(G.context));

        img_banner1 = findViewById(R.id.img_banner1_mainActivity);
        img_banner2 = findViewById(R.id.img_banner2_mainActivity);
        img_banner3 = findViewById(R.id.img_banner3_mainActivity);
        img_banner4 = findViewById(R.id.img_banner4_mainActivity);

        drawerLayout = findViewById(R.id.drawerLayout);
        drawerLayout.closeDrawer(Gravity.RIGHT);

        img_hamburger_menu = findViewById(R.id.img_hamburger_menu_mainActivity);
        img_hamburger_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.openDrawer(Gravity.RIGHT);
            }
        });

        img_search = findViewById(R.id.img_search_toolbar_mainActivity);
        img_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(G.context, Activity_Search.class);
                startActivity(intent);
            }
        });

        img_shopping = findViewById(R.id.img_shopping_toolbar_mainActivity);

        rv_superPRODUCTS = findViewById(R.id.rv_mainActivity_super_list);
        rv_superPRODUCTS.setLayoutManager(new LinearLayoutManager(G.context, LinearLayoutManager.HORIZONTAL, false));


        rv_newestPRODUCTS = findViewById(R.id.rv_mainActivity_newProducts_list);
        rv_newestPRODUCTS.setLayoutManager(new LinearLayoutManager(G.context, LinearLayoutManager.HORIZONTAL, false));

        rv_categoryList = findViewById(R.id.rv_categoryList_mainActivity);
        rv_categoryList.setLayoutManager(new LinearLayoutManager(G.context, LinearLayoutManager.HORIZONTAL, false));
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (drawerLayout.isDrawerOpen(Gravity.RIGHT)) {
            drawerLayout.closeDrawer(Gravity.RIGHT);
        }

        checkUserEmail();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == LOGIN_REQUEST && resultCode == RESULT_OK) {
            User_email = data.getExtras().getString("email");
            txt_login_navigation.setText(User_email);
            txt_login_navigation.setBackgroundColor(Color.TRANSPARENT);
            mainViewModel.saveUserEmail(User_email);
        }
        if (drawerLayout.isDrawerOpen(Gravity.RIGHT)) {
            drawerLayout.closeDrawer(Gravity.RIGHT);
        }
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(Gravity.RIGHT)) {
            drawerLayout.closeDrawer(Gravity.RIGHT);
        } else {
            super.onBackPressed();
        }


    }

    @Override
    protected void onDestroy() {
        compositeDisposable.dispose();
        timer.purge();
        timer.cancel();
        super.onDestroy();
    }
}
