package com.example.digikala.Activity_Main.MVVM;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.digikala.G;
import com.example.digikala.Model.Banner;
import com.example.digikala.Model.Carousel;
import com.example.digikala.Model.Cat;
import com.example.digikala.Model.Message.Messages;
import com.example.digikala.Model.MyTimer;
import com.example.digikala.Model.Product;

import java.util.List;

import io.reactivex.Single;

public class UserLoginInfo implements MainDataSource {

    SharedPreferences sharedPreferences;

    public UserLoginInfo(Context context) {
        sharedPreferences = context.getSharedPreferences("Login",Context.MODE_PRIVATE);
    }

    public void save_UserEmail(String email){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("email",email);
        editor.apply();
    }

    public void remove_UserEmail(String email){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.commit();
    }

    @Override
    public Single<List<Product>> get_SuperProduct_MDS() {
        return null;
    }

    @Override
    public Single<List<Product>> get_NewestProduct_MDS() {
        return null;
    }

    @Override
    public Single<List<Banner>> get_Banner_MDS() {
        return null;
    }

    @Override
    public Single<List<Carousel>> get_Slider_MDS() {
        return null;
    }

    @Override
    public Single<MyTimer> get_timer_MDS() {
        return null;
    }

    @Override
    public Single<List<Cat>> get_categryList_MDS() {
        return null;
    }

    @Override
    public String get_UserEmail() {
        return sharedPreferences.getString("email","");
    }

    @Override
    public Single<Messages> getCountBasket_MDS(String email) {
        return null;
    }
}
