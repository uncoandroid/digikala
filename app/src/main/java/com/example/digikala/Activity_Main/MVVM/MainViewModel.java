package com.example.digikala.Activity_Main.MVVM;

import com.example.digikala.Detail.MVVM.ProductDetailViewModel;
import com.example.digikala.G;
import com.example.digikala.Model.Banner;
import com.example.digikala.Model.Cat;
import com.example.digikala.Model.Message.Messages;
import com.example.digikala.Model.MyTimer;
import com.example.digikala.Model.Product;
import com.example.digikala.Model.Carousel;

import java.util.List;

import io.reactivex.Single;

public class MainViewModel {
    MainRepository mainRepository = new MainRepository();
    UserLoginInfo userLoginInfo = new UserLoginInfo(G.context);
    ProductDetailViewModel productDetailViewModel = new ProductDetailViewModel();

    public Single<List<Product>> get_SuperProduct_MVM() {
        return mainRepository.get_SuperProduct_MDS();
    }

    public Single<List<Banner>> get_Banner_MVM() {
        return mainRepository.get_Banner_MDS();

    }

    public Single<List<Carousel>> get_slider_MVM() {
        return mainRepository.get_Slider_MDS();
    }

    public Single<List<Product>> get_NewestProdct_MVM() {
        return mainRepository.get_NewestProduct_MDS();
    }

    public Single<MyTimer> get_timer_MVM() {
        return mainRepository.get_timer_MDS();
    }

    public Single<Messages> get_basketCount_MVM(String email) {
        return mainRepository.getCountBasket_MDS(email);
    }
    public Single<List<Cat>> get_CategoryListMVM() {
        return mainRepository.get_categryList_MDS();
    }

    public String getUserEmail() {
        return mainRepository.get_UserEmail();
    }

    public void saveUserEmail(String email) {
        userLoginInfo.save_UserEmail(email);
    }

    public void remove_UserEmail(String email) {
        userLoginInfo.remove_UserEmail(email);
    }

}
