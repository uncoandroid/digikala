package com.example.digikala.Activity_Main.MVVM;

import com.example.digikala.G;
import com.example.digikala.Model.Banner;
import com.example.digikala.Model.Cat;
import com.example.digikala.Model.Message.Messages;
import com.example.digikala.Model.MyTimer;
import com.example.digikala.Model.Product;
import com.example.digikala.Model.Carousel;

import java.util.List;

import io.reactivex.Single;

public class MainRepository implements MainDataSource {

    private MainApiService apiMainDataSource = new MainApiService();
    private UserLoginInfo userLoginInfo = new UserLoginInfo(G.context);

    @Override
    public Single<List<Product>> get_SuperProduct_MDS() {
        return apiMainDataSource.get_SuperProduct_MDS();
    }

    @Override
    public Single<List<Product>> get_NewestProduct_MDS() {
        return apiMainDataSource.get_NewestProduct_MDS();
    }

    @Override
    public Single<List<Banner>> get_Banner_MDS() {
        return apiMainDataSource.get_Banner_MDS();
    }

    @Override
    public Single<List<Carousel>> get_Slider_MDS() {
        return apiMainDataSource.get_Slider_MDS();
    }

    @Override
    public Single<MyTimer> get_timer_MDS() {
        return apiMainDataSource.get_timer_MDS();
    }

    @Override
    public Single<List<Cat>> get_categryList_MDS() {
        return apiMainDataSource.get_categryList_MDS();
    }

    @Override
    public String get_UserEmail() {
        return userLoginInfo.get_UserEmail();
    }

    @Override
    public Single<Messages> getCountBasket_MDS(String email) {
        return apiMainDataSource.getCountBasket_MDS(email);
    }


}
