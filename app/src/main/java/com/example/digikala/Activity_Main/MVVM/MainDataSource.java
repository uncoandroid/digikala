package com.example.digikala.Activity_Main.MVVM;

import com.example.digikala.Model.Banner;
import com.example.digikala.Model.Cat;
import com.example.digikala.Model.Message.Messages;
import com.example.digikala.Model.MyTimer;
import com.example.digikala.Model.Product;
import com.example.digikala.Model.Carousel;

import java.util.List;

import io.reactivex.Single;

public interface MainDataSource {

    Single<List<Product>> get_SuperProduct_MDS();
    Single<List<Product>> get_NewestProduct_MDS();
    Single<List<Banner>> get_Banner_MDS();
    Single<List<Carousel>> get_Slider_MDS();
    Single<MyTimer> get_timer_MDS();
    Single<List<Cat>> get_categryList_MDS();
    String get_UserEmail();
    Single<Messages> getCountBasket_MDS(String email);

}
