package com.example.digikala.Activity_Main.MVVM;

import com.example.digikala.Model.Banner;
import com.example.digikala.Model.Cat;
import com.example.digikala.Model.Message.Messages;
import com.example.digikala.Model.MyTimer;
import com.example.digikala.Model.Product;
import com.example.digikala.Model.Carousel;
import com.example.digikala.Retrofit.ApiProvider;
import com.example.digikala.Retrofit.ApiService;

import java.util.List;

import io.reactivex.Single;

public class MainApiService implements MainDataSource {


    // **** saeed  taavajoh ****  public 1 va public 2 ba ham farghi nadare ****

    public ApiService apiService =ApiProvider.apiProvider();

    @Override
    public Single<List<Product>> get_SuperProduct_MDS() {
        return ApiProvider.apiProvider().get_superProducts_php();
    }

    @Override
    public Single<List<Product>> get_NewestProduct_MDS() {
        return ApiProvider.apiProvider().get_superProducts_php();
    }

    @Override
    public Single<List<Banner>> get_Banner_MDS() {
        return apiService.get_Banner_php();
    }

    @Override
    public Single<List<Carousel>> get_Slider_MDS() {
        return apiService.get_slider_php();
    }

    @Override
    public Single<MyTimer> get_timer_MDS() {
        return apiService.get_timer_php();
    }

    @Override
    public Single<List<Cat>> get_categryList_MDS() {
        return apiService.get_Category_php();
    }

    @Override
    public String get_UserEmail() {
        return null;
    }

    @Override
    public Single<Messages> getCountBasket_MDS(String email) {
        return ApiProvider.apiProvider().getbasketcount_php(email);
    }

}


