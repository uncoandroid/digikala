package com.example.digikala.Adapter;

import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.digikala.G;
import com.example.digikala.Model.Properties;
import com.example.digikala.R;

import java.util.List;

public class Adapter_Properties extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    TextView title;
    TextView txt_key, txt_value;
    ImageView img_haveOrHas;
    private static final int TITLE = 1;
    private static final int PARAM_VALUE = 2;
    List<Properties> propertiesList;

    public Adapter_Properties(List<Properties> propertiesList) {
        this.propertiesList = propertiesList;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(G.context);
        if (viewType == TITLE) {
            View view = layoutInflater.inflate(R.layout.adapter_properties_title, parent, false);
            return new Title_ViewHolder(view);
        } else {
            View view = layoutInflater.inflate(R.layout.adapter_properties_param, parent, false);
            return new Value_ViewHolder(view);
        }

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        Properties properties = propertiesList.get(position);

        if (getItemViewType(position) == TITLE) {
            title.setText(properties.getTitle());
        } else {

            txt_key.setText(properties.getTitle());

            if (properties.getValue().equals("دارد")) {

                img_haveOrHas.setVisibility(View.VISIBLE);
                txt_value.setVisibility(View.GONE);
                img_haveOrHas.setImageDrawable(ContextCompat.getDrawable(G.context, R.drawable.ic_done));

            }else if (properties.getValue().equals("ندارد")){


                img_haveOrHas.setVisibility(View.VISIBLE);
                txt_value.setVisibility(View.GONE);
                img_haveOrHas.setImageDrawable(ContextCompat.getDrawable(G.context,R.drawable.ic_close_red));

            }else{
                txt_value.setVisibility(View.VISIBLE);
                img_haveOrHas.setVisibility(View.GONE);
                txt_value.setText(properties.getValue());
            }


        }

        Typeface typeface = Typeface.createFromAsset(G.context.getAssets(), "b_yekan.ttf");
        title.setTypeface(typeface);


    }


    @Override
    public int getItemViewType(int position) {
        if (propertiesList.get(position).getValue().equals("")) {
            return TITLE;
        } else {
            return PARAM_VALUE;
        }
    }

    @Override
    public int getItemCount() {
        return propertiesList.size();
    }

    public class Title_ViewHolder extends RecyclerView.ViewHolder {

        public Title_ViewHolder(@NonNull View v) {
            super(v);
            title = v.findViewById(R.id.txt_adapter_title_properties);
        }
    }

    public class Value_ViewHolder extends RecyclerView.ViewHolder {


        public Value_ViewHolder(@NonNull View v) {
            super(v);
            img_haveOrHas = v.findViewById(R.id.img_properties_haveOrHas);
            txt_key = v.findViewById(R.id.txt_properties_param_key);
            txt_value = v.findViewById(R.id.txt_properties_param_value);
        }
    }

}
