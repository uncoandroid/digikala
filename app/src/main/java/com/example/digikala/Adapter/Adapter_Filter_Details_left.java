package com.example.digikala.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.recyclerview.widget.RecyclerView;

import com.example.digikala.G;
import com.example.digikala.Model.Value;
import com.example.digikala.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Adapter_Filter_Details_left extends RecyclerView.Adapter<Adapter_Filter_Details_left.Detail_Left> {
    List<Value> valueList;
    OnLeftItemClickListener onLeftItemClickListener;
    String parent;


    public Adapter_Filter_Details_left(OnLeftItemClickListener onLeftItemClickListener) {
        this.onLeftItemClickListener = onLeftItemClickListener;
        this.valueList = new ArrayList<>();
    }

    public void onBindValues(List<Value> values, String parent) {
        this.valueList = values;
        this.parent = parent;
        notifyDataSetChanged();

    }


    @NonNull
    @Override
    public Detail_Left onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(G.context).inflate(R.layout.adapter_filter_details_left, parent, false);
        return new Detail_Left(view);
    }


    @Override
    public void onBindViewHolder(@NonNull final Detail_Left holder, final int position) {

        final Value value = valueList.get(position);
        holder.title.setText(value.getTitle());
        holder.checkBox.setChecked(value.isChecked());
        holder.parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                value.setChecked(!value.isChecked());
                valueList.set(position, value);
                holder.checkBox.setChecked(value.isChecked());
                JSONObject jsonObject = new JSONObject();
                try {
                    if (value.isChecked()) {
                        jsonObject.put("parent", parent);
                        jsonObject.put("title", value.getTitle());
                    }
                    jsonObject.put("parent", parent);
                    jsonObject.put("title", value.getTitle());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                onLeftItemClickListener.onLeftItemClick(valueList, parent,jsonObject);
            }
        });

        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (buttonView.isPressed()) {
                    //set check by user
                    value.setChecked(!value.isChecked());
                    valueList.set(position, value);
                    holder.checkBox.setChecked(value.isChecked());
                    JSONObject jsonObject = new JSONObject();
                    try {
                        if (value.isChecked()) {
                            jsonObject.put("parent", parent);
                            jsonObject.put("title", value.getTitle());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    onLeftItemClickListener.onLeftItemClick(valueList, parent,jsonObject);
                } else {
                    return;
                }
            }
        });

    }


    @Override
    public int getItemCount() {
        return valueList.size();
    }


    public class Detail_Left extends RecyclerView.ViewHolder {
        TextView title;
        CheckBox checkBox;
        LinearLayout parent;

        public Detail_Left(@NonNull View v) {
            super(v);
            title = v.findViewById(R.id.txt_filter_details_title_left);
            checkBox = v.findViewById(R.id.checkbox_filter_details_left);
            parent = v.findViewById(R.id.rel_leftItem_parent);
        }
    }


    public interface OnLeftItemClickListener {
        void onLeftItemClick(List<Value> values, String parent,JSONObject jsonObject);
    }
}
