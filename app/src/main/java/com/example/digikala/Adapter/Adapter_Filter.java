package com.example.digikala.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.digikala.G;
import com.example.digikala.Model.Product;
import com.example.digikala.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class Adapter_Filter extends RecyclerView.Adapter<Adapter_Filter.Filter_Viewholder> {
    List<Product> productList;

    public Adapter_Filter(List<Product> productList) {
        this.productList = productList;
    }

    @NonNull
    @Override
    public Filter_Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
       View view = LayoutInflater.from(G.context).inflate(R.layout.adapter_filter_item,parent,false);
       return  new Filter_Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Filter_Viewholder holder, int position) {
        Product product = productList.get(position);

        Picasso.get().load(product.getImage()).into(holder.img_filter_img);
        holder.txt_product_title_filter.setText(product.getTitle());
        holder.txt_product_subTitle_filter.setText(product.getTitle());
        holder.txt_product_price_filter.setText(product.getPrice());
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    public class Filter_Viewholder extends RecyclerView.ViewHolder{

        ImageView img_filter_img;
        TextView txt_product_title_filter , txt_product_subTitle_filter , txt_product_price_filter;
        public Filter_Viewholder(@NonNull View v) {
            super(v);
            img_filter_img = v.findViewById(R.id.img_filter_img);
            txt_product_title_filter = v.findViewById(R.id.txt_product_title_filter);
            txt_product_subTitle_filter = v.findViewById(R.id.txt_product_subTitle_filter);
            txt_product_price_filter = v.findViewById(R.id.txt_product_price_filter);
        }
    }

}
