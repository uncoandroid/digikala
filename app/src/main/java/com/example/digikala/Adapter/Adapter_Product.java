package com.example.digikala.Adapter;

import android.content.Intent;
import android.graphics.Typeface;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.StrikethroughSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.digikala.G;
import com.example.digikala.Model.Product;
import com.example.digikala.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class Adapter_Product extends RecyclerView.Adapter<Adapter_Product.SuperProduct_Viewholder> {

    List<Product> productList;
    onProductClick onProductClick;

    public Adapter_Product(List<Product> productList, Adapter_Product.onProductClick onProductClick) {
        this.productList = productList;
        this.onProductClick = onProductClick;
    }

    @NonNull
    @Override
    public SuperProduct_Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(G.context).inflate(R.layout.adapter_super_product, parent, false);
        return new SuperProduct_Viewholder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull SuperProduct_Viewholder holder, int position) {
        final Product product = productList.get(position);
        Picasso.get().load(product.getImage()).into(holder.logo);
        holder.title.setText(product.getTitle());

        holder.price.setText(product.getPrice());

        SpannableString spannableString = new SpannableString(product.getPprice());
        spannableString.setSpan(new StrikethroughSpan(), 0, product.getPprice().length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        holder.prevprice.setText(spannableString);

        Typeface typeface = Typeface.createFromAsset(G.context.getAssets(), "b_yekan.ttf");
        holder.title.setTypeface(typeface);

        holder.click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onProductClick.onClick(product.getId(),product.getTitle(),product.getImage());
            }
        });



    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    public class SuperProduct_Viewholder extends RecyclerView.ViewHolder {

        TextView title, price, prevprice;
        ImageView logo;
        LinearLayout click;

        public SuperProduct_Viewholder(@NonNull View v) {
            super(v);
            title = v.findViewById(R.id.txt_product_title_main_activity);
            prevprice = v.findViewById(R.id.txt_product_prevprice_main_activity);
            price = v.findViewById(R.id.txt_product_price_main_activity);
            logo = v.findViewById(R.id.img_superProduct_main_activity);
            click = v.findViewById(R.id.lin_click_product);
        }
    }

    public interface onProductClick {
        void onClick(String id ,String title ,String image);
    }
}
