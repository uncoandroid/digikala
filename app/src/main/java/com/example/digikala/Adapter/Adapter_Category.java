package com.example.digikala.Adapter;

import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.digikala.G;
import com.example.digikala.Model.Cat;
import com.example.digikala.R;

import java.lang.reflect.Type;
import java.util.List;

public class Adapter_Category extends RecyclerView.Adapter<Adapter_Category.Category_Viewholder> {

    public static List<Cat> catListt;
    OnCategoriesClick onCategoriesClick;


    public Adapter_Category(List<Cat> catList, OnCategoriesClick onCategoriesClick) {
        this.catListt = catList;
        this.onCategoriesClick = onCategoriesClick;
    }

    @NonNull
    @Override
    public Category_Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
       View view = LayoutInflater.from(G.context).inflate(R.layout.adapter_category,parent,false);
        return new Category_Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Category_Viewholder holder, final int position) {
        final Cat cat = catListt.get(position);
        holder.catTitle.setText(cat.getTitle());

        Typeface typeface = Typeface.createFromAsset(G.context.getAssets(),"cas.TTF");
        holder.catTitle.setTypeface(typeface);
        holder.Click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onCategoriesClick.OnClick(position,cat.getTitle());
            }
        });


    }

    @Override
    public int getItemCount() {
        return catListt.size();
    }

    public class Category_Viewholder extends RecyclerView.ViewHolder{

        TextView catTitle;
        LinearLayout Click;
        public Category_Viewholder(@NonNull View v) {
            super(v);
            catTitle=v.findViewById(R.id.txt_category_title);
            Click=v.findViewById(R.id.lin_category_click);
        }
    }

    public interface OnCategoriesClick{
        void OnClick(int position,String title);
    }
}
