package com.example.digikala.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.digikala.G;
import com.example.digikala.Model.Properties;
import com.example.digikala.R;

import java.util.List;

public class Adapter_ProductCompare extends RecyclerView.Adapter {

    List<Properties> originalsProperties;



    TextView txt_title,txt_compareItem_key,txt_compareItem_value,txt_compareItem_second;
    private static final int TITLE = 1000;
    private static final int ITEM = 2000;

    public Adapter_ProductCompare(List<Properties> originalsProperties) {
        this.originalsProperties = originalsProperties;

    }
    public  void  bindSecondProduct(List<Properties> secondProperties){
        originalsProperties = secondProperties;
        notifyDataSetChanged();
    }
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(G.context);
        if (viewType == TITLE) {
            View view = layoutInflater.inflate(R.layout.adapter_properties_title, parent, false);
            return new TitleViewholder(view);
        } else {
            View view = layoutInflater.inflate(R.layout.adapter_compare_product, parent, false);
            return new ItemViewholder(view);

        }
    }

    @Override
    public int getItemViewType(int position) {
        if (originalsProperties.get(position).getValue().equals("")) {
            return TITLE;
        } else {
            return ITEM;
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        Properties properties = originalsProperties.get(position);

        if (getItemViewType(position) == TITLE){
            txt_title.setText(properties.getTitle());
        }else {
            txt_compareItem_key.setText(properties.getTitle());
            txt_compareItem_value.setText(properties.getValue());
            if (properties.getSecond().equals("")){
                txt_compareItem_second.setText("");
            }else{
                txt_compareItem_second.setText(properties.getSecond());
            }
        }
    }

    @Override
    public int getItemCount() {
        return originalsProperties.size();
    }

    public class ItemViewholder extends RecyclerView.ViewHolder {

        public ItemViewholder(@NonNull View v) {
            super(v);
            txt_compareItem_key=v.findViewById(R.id.txt_compareItem_key);
            txt_compareItem_second=v.findViewById(R.id.txt_compareItem_second);
            txt_compareItem_value=v.findViewById(R.id.txt_compareItem_value);
        }
    }

    public class TitleViewholder extends RecyclerView.ViewHolder {

        public TitleViewholder(@NonNull View v) {
            super(v);
            txt_title = v.findViewById(R.id.txt_adapter_title_properties);
        }
    }
}
