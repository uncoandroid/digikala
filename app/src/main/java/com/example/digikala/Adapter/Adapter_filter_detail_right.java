package com.example.digikala.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.digikala.G;
import com.example.digikala.Model.FilterItem;
import com.example.digikala.Model.Value;
import com.example.digikala.R;

import java.util.ArrayList;
import java.util.List;

public class Adapter_filter_detail_right extends RecyclerView.Adapter<Adapter_filter_detail_right.filterRight_ViewHolder> {
    List<FilterItem> filterItemList;
    onFilterItem_Right_Click onFilterItem_right_click;
    List<Value> passValue;
    int rowIndex;



    public Adapter_filter_detail_right(List<FilterItem> filterItemList, onFilterItem_Right_Click onFilterItem_right_click) {
        this.filterItemList = filterItemList;
        this.onFilterItem_right_click = onFilterItem_right_click;
        passValue = new ArrayList<>();
    }



    public void onBindCount(List<Value> values, String parent) {
        for (int i = 0; i < filterItemList.size(); i++) {
            if (filterItemList.get(i).getTitle().equals(parent)) {
                filterItemList.get(i).setValues(values);
                passValue = filterItemList.get(i).getValues();
                notifyDataSetChanged();
            }
        }
    }



    @NonNull
    @Override
    public filterRight_ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(G.context).inflate(R.layout.adapter_filter_details_right, parent, false);
        return new filterRight_ViewHolder(view);
    }



    @Override
    public void onBindViewHolder(@NonNull filterRight_ViewHolder holder, final int position) {
        final FilterItem filterItem = filterItemList.get(position);
        holder.txt_title.setText(filterItem.getTitle());
        List<Value> values = filterItem.getValues();
        int count = 0;
        for (int j = 0; j < values.size(); j++) {
            if (values.get(j).isChecked()) {
                count++;
                holder.txt_count.setText(count + "");
            }
        }



        if (holder.count == 0) {
            holder.txt_count.setVisibility(View.GONE);
        } else {
            holder.txt_count.setVisibility(View.VISIBLE);
        }
        holder.parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rowIndex = position;
                List<Value> testValues = filterItemList.get(position).getValues();
                String testTitle = filterItem.getTitle();
                onFilterItem_right_click.onItemClick(testValues, testTitle);
                notifyDataSetChanged();
            }
        });

        if (rowIndex == position) {
            if (count > 0) {
                holder.txt_count.setVisibility(View.VISIBLE);
            } else {
                holder.txt_count.setVisibility(View.GONE);

            }
            holder.parent.setBackgroundColor(ContextCompat.getColor(G.context, R.color.colorWhite));
            holder.txt_title.setTextColor(ContextCompat.getColor(G.context, R.color.colorBlack));
            holder.txt_count.setBackground(ContextCompat.getDrawable(G.context, R.drawable.shape_adapter_filter_details_right_black));
            holder.txt_count.setTextColor(ContextCompat.getColor(G.context, R.color.colorWhite));
        } else {
            if (count > 0) {
                holder.txt_count.setVisibility(View.VISIBLE);
            } else {
                holder.txt_count.setVisibility(View.GONE);

            }
            holder.parent.setBackgroundColor(ContextCompat.getColor(G.context, R.color.filterColor));
            holder.txt_title.setTextColor(ContextCompat.getColor(G.context, R.color.colorWhite));
            holder.txt_count.setBackground(ContextCompat.getDrawable(G.context, R.drawable.shape_adapter_filter_details_right));
            holder.txt_count.setTextColor(ContextCompat.getColor(G.context, R.color.colorBlack));

        }
    }




    @Override
    public int getItemCount() {
        return filterItemList.size();
    }




    public class filterRight_ViewHolder extends RecyclerView.ViewHolder {
        TextView txt_title, txt_count;
        LinearLayout parent;
        int count;
        public filterRight_ViewHolder(@NonNull View v) {
            super(v);
            txt_title = v.findViewById(R.id.txt_filter_details_title_right);
            txt_count = v.findViewById(R.id.txt_filter_details_number_right);
            parent = v.findViewById(R.id.rel_rightItem_parent);
        }
    }



    public interface onFilterItem_Right_Click {
        void onItemClick(List<Value> values, String title);
    }
}
