package com.example.digikala.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.digikala.G;
import com.example.digikala.Model.Favorite;
import com.example.digikala.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class Adapter_FavList extends RecyclerView.Adapter<Adapter_FavList.Fav_Viewholder> {

    List<Favorite> favoriteList;
    OnFavioriteClick onFavioriteClick;
    OnDeleteClick onDeleteClick;

    public Adapter_FavList(List<Favorite> favoriteList , OnFavioriteClick onFavioriteClick,  OnDeleteClick onDeleteClick) {
        this.favoriteList = favoriteList;
        this.onFavioriteClick = onFavioriteClick;
        this.onDeleteClick = onDeleteClick;
    }

    @NonNull
    @Override
    public Fav_Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(G.context).inflate(R.layout.adapter_fav_list, parent, false);
        return new Fav_Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Fav_Viewholder holder, final int position) {
        final Favorite favorite = favoriteList.get(position);
        Picasso.get().load(favorite.getImage()).into(holder.img_logo);
        holder.favId = favorite.getFavId();
        holder.txt_title.setText(favorite.getTitle());
        holder.parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onFavioriteClick.onFavClick(favorite.getProductId(),favorite.getTitle(),favorite.getImage());
            }
        });
        holder.txt_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onDeleteClick.onDelete(favorite.getFavId());
                favoriteList.remove(position);
                notifyItemRemoved(position);
                notifyItemRangeChanged(position,favoriteList.size());
            }
        });
    }

    @Override
    public int getItemCount() {
        return favoriteList.size();
    }

    public class Fav_Viewholder extends RecyclerView.ViewHolder {
        String favId;
        LinearLayout parent;
        ImageView img_logo;
        TextView txt_title, txt_delete;

        public Fav_Viewholder(@NonNull View v) {
            super(v);
            parent = v.findViewById(R.id.card_favItem_parent);
            img_logo = v.findViewById(R.id.img_favItem_image);
            txt_title = v.findViewById(R.id.txt_favItem_title);
            txt_delete = v.findViewById(R.id.txt_favitem_delete);
        }
    }

    public interface OnFavioriteClick {
        void onFavClick(String productID , String title ,String image);
    }

    public interface OnDeleteClick {
        void onDelete(String favID);
    }
}
