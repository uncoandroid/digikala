package com.example.digikala.Adapter;

import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.digikala.G;
import com.example.digikala.Model.Comment;
import com.example.digikala.Model.RatingModel;
import com.example.digikala.R;
import com.hedgehog.ratingbar.RatingBar;

import java.util.List;

public class Adapter_AddComment extends RecyclerView.Adapter<Adapter_AddComment.AddCommentViewholder> {

    List<RatingModel> ratingModelList;

    public Adapter_AddComment(List<RatingModel> ratingModelList) {
        this.ratingModelList = ratingModelList;
    }

    @NonNull
    @Override
    public AddCommentViewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(G.context).inflate(R.layout.adapter_add_comment,parent,false);
        return new AddCommentViewholder(view);
    }

    public List<RatingModel> getRatingModelList(){
        return ratingModelList;
    }
    @Override
    public void onBindViewHolder(@NonNull AddCommentViewholder holder, int position) {
        final RatingModel ratingModel = ratingModelList.get(position);
        holder.txt_addCommentTitle.setText(ratingModel.getTitle());

        Typeface typeface = Typeface.createFromAsset(G.context.getAssets(), "cas.TTF");
        holder.txt_addCommentTitle.setTypeface(typeface);

       holder.rating_addComment.setStar(3);
       ratingModel.setValue(3+"");
       holder.rating_addComment.setOnRatingChangeListener(new RatingBar.OnRatingChangeListener() {
           @Override
           public void onRatingChange(float RatingCount) {
               ratingModel.setValue(RatingCount+"");
           }
       });
    }

    @Override
    public int getItemCount() {
        return ratingModelList.size();
    }

    public class AddCommentViewholder extends RecyclerView.ViewHolder{

        TextView txt_addCommentTitle;
        RatingBar rating_addComment;
        public AddCommentViewholder(@NonNull View v) {
            super(v);
            txt_addCommentTitle=v.findViewById(R.id.txt_addCommentTitle);
            rating_addComment=v.findViewById(R.id.rating_addComment);
        }
    }

}
