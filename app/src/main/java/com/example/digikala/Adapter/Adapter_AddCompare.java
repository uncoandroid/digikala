package com.example.digikala.Adapter;

import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.digikala.G;
import com.example.digikala.Model.Product;
import com.example.digikala.R;
import com.squareup.picasso.Picasso;

import java.util.List;


public class Adapter_AddCompare extends RecyclerView.Adapter<Adapter_AddCompare.AddCompareViewholder> {

    List<Product> productList;
    OnSearchedItemClickListener onSearchedItemClickListener;

    public Adapter_AddCompare(List<Product> productList, OnSearchedItemClickListener onSearchedItemClickListener) {
        this.productList = productList;
        this.onSearchedItemClickListener = onSearchedItemClickListener;
    }

    @NonNull
    @Override
    public AddCompareViewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(G.context).inflate(R.layout.adapter_add_compare, parent, false);
        return new AddCompareViewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AddCompareViewholder holder, int position) {
        final Product product = productList.get(position);
        Picasso.get().load(product.getImage()).into(holder.img_addCompareProduct);
        holder.txt_addCompareTitle.setText(product.getTitle());
        holder.txt_addCompareProductPrice.setText(product.getPrice());
        holder.lin_parentClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              onSearchedItemClickListener.onClickListener(product.getProperties(),product.getImage(),product.getId(),product.getTitle());
            }
        });


    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    public class AddCompareViewholder extends RecyclerView.ViewHolder {

        ImageView img_addCompareProduct;
        TextView txt_addCompareTitle, txt_addCompareProductPrice;
        LinearLayout lin_parentClick;

        public AddCompareViewholder(@NonNull View v) {
            super(v);
            txt_addCompareProductPrice = v.findViewById(R.id.txt_addCompareProductPrice);
            txt_addCompareTitle = v.findViewById(R.id.txt_addCompareTitle);
            img_addCompareProduct = v.findViewById(R.id.img_addCompareProduct);
            lin_parentClick = v.findViewById(R.id.lin_parentClick);
        }
    }

    public interface OnSearchedItemClickListener {
        void onClickListener(String properties, String image, String id,String title);
    }

}
