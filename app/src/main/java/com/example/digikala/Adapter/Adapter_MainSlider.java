package com.example.digikala.Adapter;

import com.example.digikala.Model.Carousel;
import com.example.digikala.Model.Sliders;

import java.util.List;

import ss.com.bannerslider.Slider;
import ss.com.bannerslider.adapters.SliderAdapter;
import ss.com.bannerslider.viewholder.ImageSlideViewHolder;

public class Adapter_MainSlider extends SliderAdapter {

    List<Carousel> sliderList;

    public Adapter_MainSlider(List<Carousel> sliderList) {
        this.sliderList = sliderList;
    }

    @Override
    public int getItemCount() {
        return sliderList.size();
    }

    @Override
    public void onBindImageSlide(int position, ImageSlideViewHolder imageSlideViewHolder) {
        imageSlideViewHolder.bindImageSlide(sliderList.get(position).getImage());
    }
}
