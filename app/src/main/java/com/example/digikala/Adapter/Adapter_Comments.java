package com.example.digikala.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.digikala.G;
import com.example.digikala.Model.Comment;
import com.example.digikala.Model.RatingModel;
import com.example.digikala.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Adapter_Comments extends RecyclerView.Adapter<Adapter_Comments.CommentViewholder> {

    List<RatingModel> ratingModelgList;
    List<Comment> commentList;

    OnLikeOrDisLike onLikeOrDisLike;


    public Adapter_Comments(List<Comment> commentList, OnLikeOrDisLike onLikeOrDisLike) {
        this.commentList = commentList;
        this.onLikeOrDisLike = onLikeOrDisLike;
        ratingModelgList = new ArrayList<>();
    }

    public void Change_LikeORDisLikeCount(Comment comment, String likeOrdisLike) {


        int position = commentList.indexOf(comment);
        Comment findComment = commentList.get(position);
        if (likeOrdisLike.equals("like")) {
            int currnetLike = Integer.parseInt(findComment.getLikecount());
            currnetLike++;
            findComment.setLikecount(currnetLike + "");
        } else {
            int currnetDislike = Integer.parseInt(findComment.getDislikecount());
            currnetDislike++;
            findComment.setDislikecount(currnetDislike + "");
        }
        notifyDataSetChanged();
        
    }

    @NonNull
    @Override
    public CommentViewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(G.context).inflate(R.layout.adapter_comment, parent, false);
        return new CommentViewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CommentViewholder holder, int position) {
        final Comment comment = commentList.get(position);
        ratingModelgList.clear();
        String param = comment.getParam();

        if (comment.getSuggest().equals("1")) {
            holder.lin_comment_isSuggested.setVisibility(View.VISIBLE);
        } else {
            holder.lin_comment_isSuggested.setVisibility(View.GONE);
        }
        holder.txt_comment_userName.setText(comment.getUser());
        holder.txt_comment_passage.setText(comment.getPassage());
        holder.txt_comment_like.setText(comment.getLikecount());
        holder.txt_comment_dislike.setText(comment.getDislikecount());
        holder.txt_Positive.setText(comment.getPositive());
        holder.txt_Negative.setText(comment.getNegative());

        try {
            JSONArray jsonArray = new JSONArray(param);
            for (int i = 0; i < jsonArray.length(); i++) {
                RatingModel ratingModel = new RatingModel();
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                String title = jsonObject.getString("title");
                String value = jsonObject.getString("value");
                ratingModel.setTitle(title);
                ratingModel.setValue(value);

                ratingModelgList.add(ratingModel);

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        holder.rv_Sub_commentList.setAdapter(new Adapter_ProductDetails_Rating(ratingModelgList));

        holder.lin_comment_like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onLikeOrDisLike.OnLikeOrDisLike(comment, "like");

            }
        });

        holder.lin_comment_dislike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onLikeOrDisLike.OnLikeOrDisLike(comment, "dislike");
            }
        });

    }

    @Override
    public int getItemCount() {
        return commentList.size();
    }

    public class CommentViewholder extends RecyclerView.ViewHolder {
        RecyclerView rv_Sub_commentList;
        LinearLayout lin_comment_isSuggested, lin_comment_like, lin_comment_dislike;
        TextView txt_comment_userName, txt_comment_like, txt_comment_dislike, txt_comment_passage, txt_Positive, txt_Negative, txt_comment_moreMessage;

        public CommentViewholder(@NonNull View v) {
            super(v);

            lin_comment_like = v.findViewById(R.id.lin_comment_like);
            lin_comment_dislike = v.findViewById(R.id.lin_comment_dislike);
            txt_comment_userName = v.findViewById(R.id.txt_comment_userName);
            txt_comment_like = v.findViewById(R.id.txt_comment_like);
            txt_comment_dislike = v.findViewById(R.id.txt_comment_dislike);
            txt_comment_passage = v.findViewById(R.id.txt_comment_description);
            txt_Positive = v.findViewById(R.id.txt_Positive);
            txt_Negative = v.findViewById(R.id.txt_Negative);
            txt_comment_moreMessage = v.findViewById(R.id.txt_comment_moreMessage);
            lin_comment_isSuggested = v.findViewById(R.id.lin_comment_isSuggested);
            rv_Sub_commentList = v.findViewById(R.id.rv_Sub_commentList);
            rv_Sub_commentList.setLayoutManager(new LinearLayoutManager(G.context, LinearLayoutManager.VERTICAL, false));
        }
    }


    public interface OnLikeOrDisLike {
        void OnLikeOrDisLike(Comment comment, String like_or_disLike);
    }


}
