package com.example.digikala.Adapter;

import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.example.digikala.G;
import com.example.digikala.Model.RatingModel;
import com.example.digikala.R;


import java.util.List;

public class Adapter_ProductDetails_Rating extends RecyclerView.Adapter<Adapter_ProductDetails_Rating.Rating_Viewholder> {

    List<RatingModel> ratingModelList;

    public Adapter_ProductDetails_Rating(List<RatingModel> ratingModelList) {
        this.ratingModelList = ratingModelList;
    }

    @NonNull
    @Override
    public Rating_Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(G.context).inflate(R.layout.adapter_product_details_rating, parent, false);
        return new Rating_Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Rating_Viewholder holder, int position) {
        RatingModel ratingModel = ratingModelList.get(position);
        holder.title.setText(ratingModel.getTitle());
        String rating = ratingModel.getValue();
        rating = rating.replace(".", "");
        holder.progressBar.setProgress(Integer.parseInt(rating));

        Typeface typeface = Typeface.createFromAsset(G.context.getAssets(),"b_yekan.ttf");
        holder.title.setTypeface(typeface);
        
    }

    @Override
    public int getItemCount() {
        return ratingModelList.size();
    }

    public class Rating_Viewholder extends RecyclerView.ViewHolder {

        TextView title;
        ProgressBar progressBar;

        public Rating_Viewholder(@NonNull View v) {
            super(v);
            title = v.findViewById(R.id.txt_productDetails_rating_title);
            progressBar = v.findViewById(R.id.prg_productDetails_rating);

        }
    }
}
