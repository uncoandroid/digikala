package com.example.digikala.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.digikala.G;
import com.example.digikala.Model.Basket;
import com.example.digikala.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class Adapter_BasketList extends RecyclerView.Adapter<Adapter_BasketList.BasketList_Viewholder> {
    List<Basket> basketList;
    OnBasketItemClick onBasketItemClick;
    OnRemoveItemBasket onRemoveItemBasket;
    OnPriceCallBack onPriceCallBack;

    public Adapter_BasketList(List<Basket> basketList,OnBasketItemClick onBasketItemClick,OnRemoveItemBasket onRemoveItemBasket,OnPriceCallBack onPriceCallBack) {
        this.onRemoveItemBasket = onRemoveItemBasket;
        this.onBasketItemClick = onBasketItemClick;
        this.onPriceCallBack = onPriceCallBack;
        this.basketList = basketList;
    }

    @NonNull
    @Override
    public BasketList_Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(G.context).inflate(R.layout.adapter_basket_list, parent, false);
        return new BasketList_Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BasketList_Viewholder holder, final int position) {
        final Basket basket = basketList.get(position);
        Picasso.get().load(basket.getImage()).into(holder.basketLogo);
        holder.txt_title.setText(basket.getTitle());
        holder.txt_garanti.setText(basket.getGuarantee());
        holder.lin_basketList_parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBasketItemClick.onClick(basket.getProductId());
            }
        });
        holder.txt_totalPrice.setText(basket.getPrice());
        holder.txt_finalPrice.setText(basket.getPrice());
        holder.txt_removeProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onRemoveItemBasket.onClick(basket.getBasketId());
                basketList.remove(position);
                notifyItemRemoved(position);
                notifyDataSetChanged();
            }
        });

        onPriceCallBack.onPrice(basket.getPrice());

    }

    @Override
    public int getItemCount() {
        return basketList.size();
    }

    public class BasketList_Viewholder extends RecyclerView.ViewHolder {
        LinearLayout lin_basketList_parent;
        ImageView basketLogo;
        TextView txt_title, txt_garanti, txt_forooshande, txt_totalPrice, txt_finalPrice, txt_removeProduct;

        public BasketList_Viewholder(@NonNull View v) {
            super(v);
            basketLogo = v.findViewById(R.id.img_basketList_product);
            lin_basketList_parent = v.findViewById(R.id.lin_basketList_parent);
            txt_title = v.findViewById(R.id.txt_basket_product_title_main_activity);
            txt_garanti = v.findViewById(R.id.txt_basket_garanti);
            txt_forooshande = v.findViewById(R.id.txt_basket_forooshande);
            txt_totalPrice = v.findViewById(R.id.txt_basket_total_price);
            txt_finalPrice = v.findViewById(R.id.txt_basket_finalPrice);
            txt_removeProduct = v.findViewById(R.id.txt_basket_remove_product);
        }
    }

    public interface OnBasketItemClick{
        void onClick(String productId);
    }

    public interface OnRemoveItemBasket{
        void onClick(String basketId);
    }
    public interface OnPriceCallBack{
        void onPrice(String price);
    }
}
