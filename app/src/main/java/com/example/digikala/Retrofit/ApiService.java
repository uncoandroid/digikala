package com.example.digikala.Retrofit;

import com.example.digikala.Model.Banner;
import com.example.digikala.Model.Basket;
import com.example.digikala.Model.Cat;
import com.example.digikala.Model.Comment;
import com.example.digikala.Model.Favorite;
import com.example.digikala.Model.Message.CommentMessage;
import com.example.digikala.Model.DetailProduct;
import com.example.digikala.Model.Message.EditProfileMessage;
import com.example.digikala.Model.Message.FilterMessage;
import com.example.digikala.Model.HistoryModel;
import com.example.digikala.Model.Message.Like_disLikeMessage;
import com.example.digikala.Model.Message.Messages;
import com.example.digikala.Model.MyTimer;
import com.example.digikala.Model.Product;
import com.example.digikala.Model.Carousel;
import com.example.digikala.Model.Properties;
import com.example.digikala.Model.RatingModel;
import com.example.digikala.Model.Message.RegisterMessage;

import org.json.JSONObject;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiService {
    @GET("getProduct.php")
    Single<List<Product>> get_superProducts_php();

    @GET("banner.php")
    Single<List<Banner>> get_Banner_php();

    @GET("slider.php")
    Single<List<Carousel>> get_slider_php();

    @GET("timer.php")
    Single<MyTimer> get_timer_php();

    @FormUrlEncoded
    @POST("login_sinIn.php")
    Single<RegisterMessage> get_SignIn_php(@Field("email") String email, @Field("password") String password);

    @FormUrlEncoded
    @POST("login_sinUp.php")
    Single<RegisterMessage> get_SignUp_php(@Field("email") String email, @Field("password") String password);

    @GET("getdetail.php")
    Single<List<DetailProduct>> get_detailsProduct_php(@Query("id") String id, @Query("user") String user);

    @GET("properties.php")
    Single<List<Properties>> get_properties_php();

    @GET("comments.php")
    Single<List<Comment>> getComment_php(@Query("id") String id);

    @GET("like.php")
    Single<Like_disLikeMessage> get_Like_php(@Query("id") String id);

    @GET("dislike.php")
    Single<Like_disLikeMessage> get_disLike_php(@Query("id") String id);

    @POST("addcomment.php")
    Single<CommentMessage> sendPoint_php(@Body List<RatingModel> ratingModels);

    @GET("addcommentparam.php")
    Single<CommentMessage> sendWriteCommentParam_php(
            @Query("title") String title
            , @Query("positive") String positive
            , @Query("negative") String negative
            , @Query("passage") String passage
            , @Query("user") String user
            , @Query("idproduct") String idproduct

    );

    @GET("search.php")
    Single<List<Product>> get_SearchProduct_php(@Query("search") String search);

    @GET("history.php")
    Single<List<HistoryModel>> get_HistoryModel_php(@Query("id") String id);

    @GET("getcat.php")
    Single<List<Cat>> get_Category_php();

    @GET("gettab.php")
    Single<List<Product>> get_CategoryTabItem_php(@Query("cat") String cat);

    @GET("filter.php")
    Single<List<Product>> getSortProduct_php(@Query("cat") String cat, @Query("sort") int sort);

    @POST("filterparam.php")
    Single<FilterMessage> sendFilterParam(@Body List<JSONObject> jsonObjects);

    @GET("editprofile.php")
    Single<EditProfileMessage> editProfile_php(
            @Query("email") String email,
            @Query("nik_name") String nik_name,
            @Query("family") String family,
            @Query("code_meli") String code_meli,
            @Query("tellephone") String tellephone,
            @Query("mobile") String mobile,
            @Query("tavalod") String tavalod,
            @Query("address") String address,
            @Query("jensiat") int jensiat,
            @Query("khabarname") int khabarname,
            @Query("level") int level
    );

    @GET("favorite.php")
    Single<Messages> addFaviorits_php(
            @Query("email") String email,
            @Query("id") String id,
            @Query("parent") int parent,
            @Query("title") String title
    );

    @GET("getfavorite.php")
    Single<List<Favorite>> get_Favorites(@Query("email") String email);

    @GET("deletefav.php")
    Single<Messages> delete_Favorites(@Query("id") String id);

    @GET("addbasket.php")
    Single<Messages> addToBasket(@Query("product_id") String product_id, @Query("email") String email);

    @GET("getbasketcount.php")
    Single<Messages> getbasketcount_php(@Query("email") String email);

    @GET("basketlist.php")
    Single<List<Basket>> basketlist_php(@Query("email") String email);

    @GET("deletebasket.php")
    Single<Messages> deletebasket_php(@Query("id") String id);

    @GET("checkout.php")
    Single<List<Basket>> updateBasket(@Query("order_id") String order_id,@Query("email") String email);

}
