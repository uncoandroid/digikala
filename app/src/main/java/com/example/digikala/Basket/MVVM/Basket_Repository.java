package com.example.digikala.Basket.MVVM;

import com.example.digikala.Model.Basket;
import com.example.digikala.Model.Message.Messages;

import java.util.List;

import io.reactivex.Single;

public class Basket_Repository implements Basket_DataSource {
    Basket_ApiService basket_apiService = new Basket_ApiService();

    @Override
    public Single<List<Basket>> get_basketlist_php_BDS(String email) {
        return basket_apiService.get_basketlist_php_BDS(email);
    }

    @Override
    public Single<Messages> remove_basketlist_php_BDS(String id) {
        return basket_apiService.remove_basketlist_php_BDS(id);
    }

    @Override
    public Single<List<Basket>> update_basketlist_php_BDS(String order_id, String email) {
        return basket_apiService.update_basketlist_php_BDS(order_id,email);
    }
}
