package com.example.digikala.Basket;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.example.digikala.Adapter.Adapter_BasketList;
import com.example.digikala.Basket.MVVM.Basket_ViewModel;
import com.example.digikala.Model.Basket;
import com.example.digikala.R;

import java.util.List;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class Activity_CheckOut extends AppCompatActivity {

    String orderId;
    RecyclerView recyclerView;
    TextView txtName, txtTransactionCode;
    Basket_ViewModel viewModel = new Basket_ViewModel();
    CompositeDisposable compositeDisposable = new CompositeDisposable();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity__check_out);

        setupViews();
        Uri uri = getIntent().getData();
        orderId = uri.getQueryParameter("order_id");
        txtTransactionCode.setText(orderId);

        observeForUpdateBasket();

    }

    private void setupViews() {
        recyclerView = findViewById(R.id.rv_checkOut_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        txtName = findViewById(R.id.txt_checkOut_userName);
        txtTransactionCode = findViewById(R.id.txt_checkOut_trancactionCode);
    }


    private void observeForUpdateBasket() {
        viewModel.update_basketlist_php_BDS(orderId, viewModel.getUserEmail())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<List<Basket>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(List<Basket> baskets) {
                        recyclerView.setAdapter(new Adapter_BasketList(baskets, new Adapter_BasketList.OnBasketItemClick() {
                            @Override
                            public void onClick(String productId) {

                            }
                        }, new Adapter_BasketList.OnRemoveItemBasket() {
                            @Override
                            public void onClick(String basketId) {

                            }
                        }, new Adapter_BasketList.OnPriceCallBack() {
                            @Override
                            public void onPrice(String price) {

                            }
                        }));
                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                });

    }


    @Override
    protected void onDestroy() {
        compositeDisposable.clear();
        super.onDestroy();
    }
}