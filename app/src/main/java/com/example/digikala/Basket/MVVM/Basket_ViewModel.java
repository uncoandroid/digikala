package com.example.digikala.Basket.MVVM;

import com.example.digikala.Activity_Main.MVVM.UserLoginInfo;
import com.example.digikala.G;
import com.example.digikala.Model.Basket;
import com.example.digikala.Model.Message.Messages;

import java.util.List;

import io.reactivex.Single;

public class Basket_ViewModel implements Basket_DataSource {

    Basket_Repository basket_repository = new Basket_Repository();
    UserLoginInfo userLoginInfo = new UserLoginInfo(G.context);

    @Override
    public Single<List<Basket>> get_basketlist_php_BDS(String email) {
        return basket_repository.get_basketlist_php_BDS(email);
    }

    @Override
    public Single<Messages> remove_basketlist_php_BDS(String id) {
        return basket_repository.remove_basketlist_php_BDS(id);
    }

    @Override
    public Single<List<Basket>> update_basketlist_php_BDS(String order_id, String email) {
        return basket_repository.update_basketlist_php_BDS(order_id,email);
    }

    public String getUserEmail(){
        return  userLoginInfo.get_UserEmail();
    }
}
