package com.example.digikala.Basket;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.digikala.Activity_Main.MainActivity;
import com.example.digikala.Adapter.Adapter_BasketList;
import com.example.digikala.Basket.MVVM.Basket_ViewModel;
import com.example.digikala.Detail.Activity_ProductDetails;
import com.example.digikala.G;
import com.example.digikala.Model.Basket;
import com.example.digikala.Model.Message.Messages;
import com.example.digikala.Profile.Fav_Detils_List.Activity_Fav;
import com.example.digikala.R;

import java.util.List;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class Activity_BasketList extends AppCompatActivity {

    CompositeDisposable compositeDisposable = new CompositeDisposable();
    Basket_ViewModel basket_viewModel = new Basket_ViewModel();
    String b_basketCount;
    TextView txt_basketCount;
    TextView txt_total_price;
    ImageView img_back_basketList;
    RecyclerView rv_basketActivity_list;
    Button btn_buyProduct;
    private int TotlaPrice = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_basket_list);

        b_basketCount = getIntent().getExtras().getString("basketCount");

        setupView();
        observeBasketList();


    }

    private void observeBasketList() {
        basket_viewModel.get_basketlist_php_BDS(basket_viewModel.getUserEmail())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<List<Basket>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(List<Basket> baskets) {
                        rv_basketActivity_list.setAdapter(new Adapter_BasketList(baskets, new Adapter_BasketList.OnBasketItemClick() {
                            @Override
                            public void onClick(String productId) {
                                Intent intent = new Intent(G.context, Activity_ProductDetails.class);
                                intent.putExtra("id", productId);
                                startActivity(intent);
                                finish();
                            }
                        }, new Adapter_BasketList.OnRemoveItemBasket() {
                            @Override
                            public void onClick(String basketId) {
                                basket_viewModel.remove_basketlist_php_BDS(basketId)
                                        .subscribeOn(Schedulers.newThread())
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe(new SingleObserver<Messages>() {
                                            @Override
                                            public void onSubscribe(Disposable d) {
                                                compositeDisposable.add(d);
                                            }

                                            @Override
                                            public void onSuccess(Messages messages) {
                                                Toast.makeText(G.context, "محصول از لیست سبد خرید حدف شد", Toast.LENGTH_SHORT).show();
                                                int currnetBasketCount = Integer.parseInt(txt_basketCount.getText().toString());
                                                currnetBasketCount--;
                                                TotlaPrice=0;
                                                if (currnetBasketCount == 0) {
                                                    txt_basketCount.setVisibility(View.GONE);
                                                    TotlaPrice=0;
                                                    txt_total_price.setVisibility(View.GONE);
                                                } else {
                                                    txt_basketCount.setVisibility(View.VISIBLE);
                                                    txt_basketCount.setText(currnetBasketCount + "");
                                                }
                                            }

                                            @Override
                                            public void onError(Throwable e) {

                                            }
                                        });
                            }
                        }, new Adapter_BasketList.OnPriceCallBack() {
                            @Override
                            public void onPrice(String price) {
                                TotlaPrice += Integer.parseInt(price);
                                txt_total_price.setText(TotlaPrice + "");
                                txt_total_price.setVisibility(View.VISIBLE);
                            }
                        }));


                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                });
    }

    private void setupView() {

        txt_total_price = findViewById(R.id.txt_total_price);
        img_back_basketList = findViewById(R.id.img_back_basketList);
        img_back_basketList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(G.context,MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
        txt_basketCount = findViewById(R.id.txt_basketCount);
        rv_basketActivity_list = findViewById(R.id.rv_basketActivity_list);
        rv_basketActivity_list.setLayoutManager(new LinearLayoutManager(G.context));
        btn_buyProduct = findViewById(R.id.btn_buyProduct);
        btn_buyProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(Intent.ACTION_VIEW, Uri.parse("https://clicksite.org/app_digi_mellat/example.php"));
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!b_basketCount.equals("0")) {
            txt_basketCount.setVisibility(View.VISIBLE);
            txt_basketCount.setText(b_basketCount);
        }else{
            Toast.makeText(this, "سبد خرید شما خلی میباشد", Toast.LENGTH_SHORT).show();
            txt_basketCount.setVisibility(View.GONE);

        }
    }

    @Override
    protected void onDestroy() {
        compositeDisposable.dispose();
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Activity_BasketList.this, MainActivity.class);
        startActivity(intent);
        super.onBackPressed();
    }
}
