package com.example.digikala.Basket.MVVM;

import com.example.digikala.Model.Basket;
import com.example.digikala.Model.Message.Messages;
import com.example.digikala.Retrofit.ApiProvider;
import com.example.digikala.Retrofit.ApiService;

import java.util.List;

import io.reactivex.Single;

public class Basket_ApiService implements Basket_DataSource {
    ApiService apiService = ApiProvider.apiProvider();


    @Override
    public Single<List<Basket>> get_basketlist_php_BDS(String email) {
        return apiService.basketlist_php(email);
    }

    @Override
    public Single<Messages> remove_basketlist_php_BDS(String id) {
        return apiService.deletebasket_php(id);
    }

    @Override
    public Single<List<Basket>> update_basketlist_php_BDS(String order_id, String email) {
        return apiService.updateBasket(order_id,email);
    }
}
