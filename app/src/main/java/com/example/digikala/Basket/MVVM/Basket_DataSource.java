package com.example.digikala.Basket.MVVM;

import com.example.digikala.Model.Basket;
import com.example.digikala.Model.Message.Messages;

import java.util.List;

import io.reactivex.Single;

public interface Basket_DataSource {
    Single<List<Basket>> get_basketlist_php_BDS(String email);
    Single<Messages> remove_basketlist_php_BDS(String id);
    Single<List<Basket>> update_basketlist_php_BDS(String order_id,String email);
}
